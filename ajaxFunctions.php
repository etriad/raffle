<?php
	include_once("inc/incConstants.php");
	include_once("shipmentModel.php");
	include_once("masterbillModel.php");	
	include_once("mailerModel.php");

    
    $fn = $get['fn'];
    
    switch($fn) {
    
    	case 'getContacts':
		
			$term = strtolower($get['term']);
			
			$sql = "SELECT * FROM CONTACTS WHERE lower(CONTACT_FNAME) LIKE '%".$term."%' OR lower(CONTACT_LNAME) LIKE '%".$term."%' OR lower(CONTACT_ADDRESS1) LIKE '%".$term."%' OR lower(CONTACT_CITY) LIKE '%".$term."%' OR lower(CONTACT_COMPANY) LIKE '%".$term."%' OR lower(CONTACT_ZIP) LIKE '%".$term."%' order by CONTACT_COMPANY ASC";
			$query = $db->prepare($sql);
			$query->execute();
			$results = $query->fetchAll();
			$arrJsonGroup = array();
			foreach($results as $result){
				array_push($arrJsonGroup,array('id'=>$result['CONTACT_ID'],'label'=>$result['CONTACT_COMPANY'].' - '.$result['CONTACT_CITY'].', '.$result['CONTACT_STATE'],'value'=>$result['CONTACT_COMPANY']));
			}
			echo json_encode($arrJsonGroup);
		break;


    	case 'getContact':
		
			$contactID = $get['id'];
			
			$sql = "SELECT * FROM CONTACTS WHERE CONTACT_ID = :contact_id LIMIT 1";
			$query = $db->prepare($sql);
			$query->bindValue(":contact_id",$contactID);
			$query->execute();
			$results = $query->fetch();
			echo json_encode($results);
		break;
    
    
		case 'deleteShipment':
		
			$shipmentID = $get['sid'];
			$sql = "UPDATE SHIPMENTSX SET STATUS = 'deleted' WHERE SHIPMENT_ID = :shipment_id LIMIT 1";
			$query = $db->prepare($sql);
			$query->bindValue(":shipment_id",$shipmentID);
			$query->execute();
			return true;
				
		break;
		
		
		case 'updateStatus':

			$type = $get['type'];
			switch ($type) {
				
				case 'shipment':	
				
					$shipmentID = $get['sid'];
					$status = $get['status'];
					$sql = "UPDATE SHIPMENTSX SET STATUS = :status ";
					if($status=='delivered'){
						$sql .= ", DATE_DELIVERED = NOW() ";
					}
					$sql .= "WHERE SHIPMENT_ID = :shipment_id LIMIT 1";
					$query = $db->prepare($sql);
					$query->bindValue(":status",$status);
					$query->bindValue(":shipment_id",$shipmentID);
					$query->execute();


						$msg = "Shipment Status has been updated to <strong>".$status."</strong>";
				  
						$shipmentStatus = $status;
				
						$toFrom = "Shipper: ".$shipment['FROM_COMPANY']." - ".$shipment['FROM_CITY'].", ".$shipment['FROM_STATE']." <br>Consignee: ".$shipment['TO_COMPANY']." - ".$shipment['TO_CITY'].", ".$shipment['TO_STATE'];
						$arrContent = array('[INSERT_TRACKINGNUMBER]'=>$shipment['TRACKING_NUMBER'],'[INSERT_SHIPMENTSTATUS]'=>strtoupper($status),'[INSERT_TOFROM]'=>$toFrom);
						
						$arrTo = array();
						if(strlen($shipment['FROM_EMAIL'])>0){
							array_push($arrTo,array('email'=>$shipment['FROM_EMAIL'],'name'=>$shipment['FROM_COMPANY']));
						}
						if(strlen($shipment['TO_EMAIL'])>0){
							array_push($arrTo,array('email'=>$shipment['TO_EMAIL'],'name'=>$shipment['TO_COMPANY']));
						}
						
						if(count($arrTo)==0){
							array_push($arrTo,array('email'=>'admin@trophytransport','Trophy Transport'));
						}
						
						sendStatusMail($arrTo,$status,$arrContent);


					return true;
					
				break;
				
				case 'masterbill':
				
					$masterbillID = $get['id'];
					$status = $get['status'];
					$sql = "UPDATE MASTERBILL SET MASTERBILL_STATUS = :status ";
					$sql .= "WHERE MASTERBILL_ID = :id LIMIT 1";
					$query = $db->prepare($sql);
					$query->bindValue(":status",$status);
					$query->bindValue(":id",$masterbillID);
					$query->execute();
					return true;
	
				break;
			
			}

		break;


		
		case 'masterbill':
		
			$masterbillID = $get['mbid'];
			$arrBOL = $get['arrBOL'];
			
			if(count($arrBOL)>0){ #bol's were passed in
				$firstShipmentID = $arrBOL[0];

				if($masterbillID == 0){
					#new masterbill
					$sql = "INSERT INTO MASTERBILL (BILL_COMPANY,BILL_ADDRESS1,BILL_ADDRESS2,BILL_CITY,BILL_STATE,BILL_ZIP,BILL_COUNTRY,BILL_FNAME,BILL_LNAME,BILL_PHONE1,BILL_PHONE2,BILL_CUSTOMER_NUMBER) (SELECT BILL_COMPANY,BILL_ADDRESS1,BILL_ADDRESS2,BILL_CITY,BILL_STATE,BILL_ZIP,BILL_COUNTRY,BILL_FNAME,BILL_LNAME,BILL_PHONE1,BILL_PHONE2,BILL_CUSTOMER_NUMBER FROM SHIPMENTSX WHERE SHIPMENT_ID = :shipmentID)";
					$query = $db->prepare($sql);
					$query->bindValue(":shipmentID",$firstShipmentID);
					$query->execute();
					$masterbillID = $db->lastInsertId();
					$query->closeCursor();
					
					#loop through and assoc. bol to masterbill
					foreach($arrBOL as $bol){
						$sql = "REPLACE INTO MASTERBILL_SHIPMENT VALUES (:masterbillID,:shipmentID)";
						$query = $db->prepare($sql);
						$query->bindValue(":masterbillID",$masterbillID);
						$query->bindValue(":shipmentID",$bol);
						$query->execute();
					}
				} else {
					#existing masterbill
					
					#loop through and assoc. bol to masterbill
					foreach($arrBOL as $bol){
						$sql = "REPLACE INTO MASTERBILL_SHIPMENT VALUES (:masterbillID,:shipmentID)";
						$query = $db->prepare($sql);
						$query->bindValue(":masterbillID",$masterbillID);
						$query->bindValue(":shipmentID",$bol);
						$query->execute();
					}
					
				}
				
				echo $masterbillID;
			
			}
		
		break;
		
		
		case 'getMasterbill':
			
			$masterbillID = $get['mbid'];
			$sql = "SELECT * FROM MASTERBILL WHERE MASTERBILL_ID = :masterbillID";
			$query = $db->prepare($sql);
			$query->bindValue(":masterbillID",$masterbillID);
			$query->execute();
			$arrMasterbill = $query->fetch();
			return $arrMasterbill;
		break;
		
		
		case 'getShipments':
			$aaData = array();
			$arrShipments = getAllShipments();
			$aaData['aaData']=$arrShipments;
			$json = json_encode($aaData);
			echo $json;
			
			#return $json;
		
		break;
		
		
		case 'removeMasterbillShipment':
			$masterbillID = $get['mbid'];
			$shipmentID = $get['sid'];
			$sql = "DELETE FROM MASTERBILL_SHIPMENT WHERE MASTERBILL_ID = :masterbillID and SHIPMENT_ID = :shipmentID";
			$query = $db->prepare($sql);
			$query->bindValue(":masterbillID",$masterbillID);
			$query->bindValue(":shipmentID",$shipmentID);
			$query->execute();
		break;
		

		case 'addMasterbillNote':
			
			$note = $get['note'];
			$mbid = $get['mbid'];
			if(isset($get['uid'])){
				$userID = $get['uid'];
			} else {
				$userID = 0;
			}
			if(isset($get['noteType'])){
				$noteType = $get['noteType'];
			} else {
				$noteType = 'pvt';
			}
			$sql = "INSERT INTO MASTERBILL_NOTES (MASTERBILL_ID,USER_ID,NOTE,NOTE_TYPE) VALUE (:masterbillID,:userID,:note,:noteType)";
			$query = $db->prepare($sql);
			$query->bindValue(":masterbillID",$mbid);
			$query->bindValue(":userID",$userID);
			$query->bindValue(":note",stripslashes($note));
			$query->bindValue(":noteType",$noteType);
			$query->execute();

			$arrNotes = getMasterbillNotes($mbid);
			echo json_encode(array('notes'=>$arrNotes));
			
		break;
		
		case 'removeMasterbillNote':
		
			$noteID = $get['nid'];
			$sql = "DELETE FROM MASTERBILL_NOTES WHERE MASTERBILL_NOTE_ID = :noteID";
			$query = $db->prepare($sql);
			$query->bindValue(":noteID",$noteID);
			$query->execute();
			
		break;


	}
 ?>