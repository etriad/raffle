<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>LFG &amp; United Way Basket Raffle</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.min.css" rel="stylesheet">
  <style type="text/css">
    html,
    body,
    header,
    .carousel {
      height: 60vh;
    }

    @media (max-width: 740px) {
      html,
      body,
      header,
      .carousel {
        height: 100vh;
      }
    }

    @media (min-width: 800px) and (max-width: 850px) {
      html,
      body,
      header,
      .carousel {
        height: 100vh;
      }
    }

    main div.container {
        margin-top: 75px;
    }

  </style>
</head>

<body>

  <!-- Navbar -->
  <nav class="navbar  navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container">

      <!-- Brand -->
      <a class="navbar-brand" href="#">
        <img src="assets/lfg_uw_mini_banner.png" height="30" alt="Lincoln Financial &amp; United Way">
      </a>

      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- Left -->
        <ul class="navbar-nav mr-auto">
        <?php if(!$loggedIn){ ?>
          <li class="nav-item  <?php if($page=="home"){ echo "active"; }?>">
            <a class="nav-link waves-effect" href="home.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
        <?php } ?>
          <li class="nav-item <?php if($page=="view_baskets"){ echo "active"; } ?>">
            <a class="nav-link waves-effect" href="view_baskets.php"> Baskets </a>
          </li>
          <li class="nav-item <?php if($page=="buy_tickets"){ echo "active"; } ?>">
            <a class="nav-link waves-effect" href="buy_tickets.php" > Buy Tickets </a>
          </li>
          <li class="nav-item">
            <a class="nav-link waves-effect" href="about.php" > About the Raffle </a>
          </li>
        </ul>

        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
        <?php if ($totalTickets > 0 && $remainingTickets > 0) { ?>
          <li class="nav-item">
            <a class="nav-link waves-effect">
              <i class="fa fa-ticket"></i>
              <span class="badge blue z-depth-1 mr-1"> <?=$remainingTickets?> </span>
              <span class="clearfix d-none d-sm-inline-block"> Tickets </span>
            </a>
          </li>
        <?php } ?>
          <?php if($loggedIn){ ?>
          <li class="nav-item">
            <a href="logout.php" class="nav-link waves-effect" target="_blank">
              Log Off <?=$_SESSION['customer_email']?>
            </a>
          </li>
          <?php } ?>


        </ul>

      </div>

    </div>
  </nav>
  <!-- Navbar -->

<?=$content?>

  <!--Footer-->
  <footer class="page-footer text-center font-small mt-4 wow fadeIn">

    <!--Call to action-->
    <div class="pt-4">
      <a class="btn btn-outline-white" href="buy_tickets.php" role="button">BUY TICKETS!
        <i class="fa fa-ticket ml-2"></i>
      </a>
      <a class="btn btn-outline-white" href="mailto:phillip.planes@lfg.com??subject=Raffle%20Site%20Help" target="_blank" role="button">NEED HELP?
        <i class="fa fa-life-buoy ml-2"></i>
      </a>
    </div>
    <!--/.Call to action-->

    <hr class="my-4">


    <!--Copyright-->
    <div class="footer-copyright py-3">
      © 2018 Copyright:
      Website Courtesy of LFG IT Infrastructure Team
    </div>
    <!--/.Copyright-->

  </footer>
  <!--/.Footer-->

  <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Initializations -->
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();
    $(function () {
        $('[data-toggle="popover"]').popover({
            trigger: 'hover',
            placement: 'auto',
            html: true
        })
    })
  </script>
</body>

</html>