<?php

session_start();

#enable show errors
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

$ipAddress = $_SERVER['REMOTE_ADDR'];
$page = "";

$sessionID = session_id();
#database connection
#$dsn = 'mysql:dbname=raffle;host=localhost';
#$dbuser = 'dbadmin';
#$dbpassword = 'WebHosting123';
$dsn = 'mysql:dbname=raffle;host=mysql';
$dbuser = 'root';
$dbpassword = 'tiger';

try {
    $db = new PDO($dsn, $dbuser, $dbpassword, array(PDO::ATTR_PERSISTENT => true));
} catch (PDOException $e) {
    exit('DB Connection failed: '. $e->getMessage());
}

$post = $_POST;
$get = $_GET;

$err = false;

include_once("functions.php");

if(isset($_SESSION['loggedIn']) && isset($_SESSION['customerID']) && $_SESSION['loggedIn']==true){

    $loggedIn = true;
    $arrTicketTotals = getCustomerTicketTotal($_SESSION['customerID']);
    $totalTickets = (int)$arrTicketTotals['total_cnt'];
    $usedTickets = (int)getCustomerTicketsUsed($_SESSION['customerID']);
    $remainingTickets = $totalTickets - $usedTickets;

} else {
 
    $loggedIn = false;
    $totalTickets = 0;
    $remainingTickets = 0;

}

if(isset($get['cid'])){
    $customerID = $get['cid'];
    $arrCustomer = getCustomer($customerID);
}

if(isset($get['iid'])){
    $itemID = $get['iid'];
    $arrItem = getItem($itemID);
}




?>
