<?php
    include_once("incConstants.php");
    secureAdmin();
    
    if(!isset($get['iid'])){
        header("location: admin_items.php");
    }

    if(isset($post['pull_winner'])){
      $arrWinner = pullItemWinner($itemID);
    }

    $itemTotalEntries = getItemTotalTickets($itemID);
    $arrEntries = getItemTickets($itemID);

  #var_dump($arrCustomer);

  #var_dump($arrCustomerTickets);

  #var_dump($getCustomerTicketTotal);

  #var_dump($getCustomerTicketsUsed);

  #var_dump($getCustomeritem_tickets);

	
	ob_start();
?>
			<div class="row">
            <div class="col-lg-3 col-md-4">
					<div class="card h-100">
						<img class="card-img-top" alt="<?php echo $arrItem['item_name']; ?>" src="items/<?php echo $arrItem['item_image']; ?>">
						<div class="card-body">
							<h5 class="card-title">
								<?php echo $arrItem['item_name']; ?>
							</h5>
							<p class="card-text">
								<?php echo $arrItem['item_desc']; ?>
							</p>
						</div>
					</div>
				</div>
			</div>
<?php
if(isset($post['pull_winner']) && $arrWinner){
  echo "<hr><h2> WINNER: ".$arrWinner['customer_email']."</h2><hr>";
}
?>
<hr>
<form method="post" action="admin_item_detail.php?iid=<?=$itemID?>">
<input type="hidden" name="pull_winner" value="true">
<input class="btn btn-lg btn-success" type="submit" value="Pull Random Winner">
</form>
<hr>
<h3>Total Entries: <?=$itemTotalEntries?></h3>


<hr>


<?php if(count($arrEntries)>0){ ?>
<h3>Raffles Entered</h3>
<table id="item_tickets" class="display" style="width:100%">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Customer</th>
      <th scope="col">Timestamp</th>

    </tr>
  </thead>
  <tbody>
  <?php foreach($arrEntries as $entry){ ?>
    <tr>
      <th scope="row"><?php echo $entry['transaction_id']; ?></th>
      <td><a href="admin_customer_detail.php?cid=<?=$entry['customer_id']?>"><?=$entry['customer_email']?></a></td>
      <td><?php echo $entry['timestamp']; ?></td>
    </tr>
  <?php } ?>
  </tbody>
  <tfoot>
    <tr>
    <th scope="col">ID</th>
      <th scope="col">Customer</th>
      <th scope="col">Timestamp</th>

    </tr>
  </tfoot>
</table>

<script type="text/javascript" class="init">
	

    $(document).ready(function() {
        $('#item_tickets').DataTable();
    } );
    
    
</script>
<?php } else { ?>
<h4>No Raffle Entries Yet</h4>
<?php } ?>

<?php
	$content = ob_get_clean();
	include_once('adminLayout.php');
?>
