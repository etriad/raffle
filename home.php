<?php

  include_once("incConstants.php");
  if(isset($_SESSION['loggedIn']) && isset($_SESSION['customerID']) && $_SESSION['loggedIn']==true){
    header("location:view_baskets.php");
  }
  $arrItems = getAllItems();
	$page = "home";
  $alert = false;
  $showCodeForm = false;

      if(isset($get['sendcode'])){
        $arrCustomer = getCustomerByEmail($_SESSION['customer_email']);
        sendEmail( 'customerCode',array('to'=>$_SESSION['customer_email'],'code'=>$arrCustomer['customer_code']) );
        $showCodeForm = true;
        $alert = true;
        $alertType = "success";
        $alertMsg = "You access code has been re-sent to ".$get['sendcode'];
      }

      if(isset($post['email'])){
          $email = strtolower($post['email']);
          if(preg_match('/@lfg\.com$/i',$email) || preg_match('/@lfd\.com$/i',$email)){

              $arrCustomer = getCustomerByEmail($email);

              if($arrCustomer){
                  $customerID = $arrCustomer['customer_id'];
                  $_SESSION['customerID'] = $customerID;
                  $_SESSION['customer_email'] = $arrCustomer['customer_email'];
                  $showCodeForm = true;
              } else {
                  $code = generateCode();
                  $customerID = addCustomer($email,$code,'','');
                  if($customerID){
                     if(sendEmail('customerCode',array("to"=>$email,"code"=>$code))){
                      $_SESSION['codeSent'] = true;                     
                    } else {
                      $alert = true;
                      $alertType = "danger";
                      $alertMsg = "Email could not send. Your personal code is $code";
                    }
                    $_SESSION['customer_email'] = $email;
                    $_SESSION['code'] = $code;
                    $_SESSION['customerID'] = $customerID;
                    $showCodeForm = true;
                  }
              } #end if is customer
              #header("Location:get_code.php");

          } else {
              $alert = true;
              $alertType = "danger";
              $alertMsg =  "Sorry, Only LFG email addresses allowed to participate.";
          }#end if lfg email
      }#end post['email'] check


      if(isset($_SESSION['codeSent'])){
        $alert=true;
        $alertType="success";
        $alertMsg = "We emailed your access code.";
        $showCodeForm = true;
      }

      #################################################
      if(isset($post['code']) && isset($_SESSION['customer_email'])){

        $code = strtolower($post['code']);

        if(checkCustomerCode($_SESSION['customerID'],$code)){
            $_SESSION['loggedIn'] = true;
            header("Location:view_baskets.php");
        } else {
            $alert = true;
            $alertType = 'danger';
            $alertMsg = "That is not the correct code.";
            $showCodeForm = true;
        }
  
      }#end post code check

ob_start();
?>	
  <!--Main layout-->
  <main>
    
    <div class="container-fluid mt-4">
    <div class="card card-image view" style="background-image: url(assets/raffle_tickets_dark.jpg);  background-repeat: no-repeat; background-size: cover;">
      <div class="text-white text-center py-2 px-4 my-2">
        <div>
          <div class="container">
          <div class="row">
          <div class="col-lg-8 offset-lg-2"
          <!-- Default form login -->
          
          <?php if($showCodeForm == true){ ?>
          
            <form class="text-center p-5" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <p class="h4 mb-4">Enter Your Access Code</p>
            <?php if($alert){ echo '<div class="alert alert-'.$alertType.'">'.$alertMsg.'</div>'; } ?>         
            <!-- Email -->
            <input type="text" name="code" autofocus id="defaultLoginFormCode" class="form-control form-control-lg mb-4 text-center" placeholder="Code">
            <!-- Sign in button -->
            <button class="btn btn-info my-2" type="submit">Submit Access Code</button>
            </form>

            <p><a href="<?php echo $_SERVER['PHP_SELF']; ?>?sendcode=<?=$_SESSION['customer_email']?>">Re-send your access code</a></p>

          <?php } else  { ?>

            <form class="text-center p-5" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <p class="h4 mb-4">Enter Your Lincoln Email to Get Started!</p>
            <?php if($alert){ echo '<div class="alert alert-'.$alertType.'">'.$alertMsg.'</div>'; } ?>         
            <!-- Email -->
            <input type="email" name="email" autofocus id="defaultLoginFormEmail" class="form-control form-control-lg mb-4 text-center" placeholder="your @lfg.com or @lfd.com email">
            <!-- Sign in button -->
            <button class="btn btn-info btn-amber my-6" type="submit">Submit Your LFG Email</button>
            </form>

          <?php } ?>
            <!-- Default form login -->

            <p class="mx-5 mb-3">Raffle entry is through virtual tickets purchased from one of <a href="buy_tickets.php">these sources.</a></p>

            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>



    <div class="container mt-5">

      <!--Section: Products v.3-->
      <section class="text-center mb-4">
          <h2>Look at these AWESOME Prizes!</h2>
          <h6>mouse-over for descriptions</h6>

        <!--Grid row-->
        <div class="row wow fadeIn">
<?php foreach($arrItems as $item){ 
	$itemTotalTickets = (int)getItemTotalTickets($item['item_id']);
?>
          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card h-100">

              <!--Card image-->
              <div class="view overlay">
              <img src="items/<?=$item['item_image']?>" class="card-img-top" alt="<?=$item['item_name']?>" data-toggle="popover" title="<?=$item['item_name']?>" data-content="<?=$item['item_desc']?>">

              </div>
              <!--Card image-->

              <!--Card content-->
              <div class="card-body text-center">
                <!--Category & Title-->

                  <h5><?=$item['item_name']?></h5>


              </div>
              <!--Card content-->

            </div>
            <!--Card-->

          </div>
          <!--Grid column-->
<?php } ?>

        </div>
        <!--Grid row-->

      </section>
      <!--Section: Products v.3-->


    </div>
  </main>
  <!--Main layout-->

<?php
	$content = ob_get_clean();
	include_once('layout.php');
?>