<?php
    include_once("incConstants.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LFG - United Way Raffle</title>

    <meta name="description" content="">
    <meta name="author" content="P.Planes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>

    <div class="container h-100">


      <nav class="navbar navbar-expand-lg navbar-light  mb-5 ">
        <a class="navbar-brand" href="#"><img class="img-fluid" src="assets/banner_logos.png"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
          <ul class="nav mr-auto">
            <li class="nav-item <?php if($page=="items"){ echo "active"; } ?>">
              <a class="nav-link"  href="items.php">SEE BASKETS</a>
            </li>
            <?php if ($totalTickets > 0 && $remainingTickets > 0) { 
                echo '<span class="navbar-text">YOU HAVE '.$remainingTickets.' TICKETS.</span>';
              } else {
                echo '<li class="nav-item"><a class="nav-link" href="buy_tickets.php">BUY TICKETS</a></li>';
              }
          ?>
          </ul>

          <?php if($loggedIn){ ?>
          <span class="navbar-text ml-2">
            <a class="lead" href="logout.php">Log Out</a>
          </span>
          <?php } ?>
        </div>
      </nav>
	



			<?php echo $content; ?>


    </div> <!-- end main container -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>