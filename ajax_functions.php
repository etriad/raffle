<?php 
    include_once("incConstants.php");

    switch($get['fn']){

        case "emailcheck":
            if(isset($post['email'])){
                $email = strtolower($post['email']);
                if(preg_match('/@lfg\.com$/i',$email) || preg_match('/@lfd\.com$/i',$email)){

                    $arrCustomer = getCustomerByEmail($email);

                    if($arrCustomer){
                        $customerID = $arrCustomer['customer_id'];
                        $_SESSION['customerID'] = $customerID;
                        $_SESSION['customer_email'] = $arrCustomer['customer_email'];
                        echo '{"status":"ok","msg":"user exists"}';

                    } else {
                        $code = generateCode();
                        $customerID = addCustomer($email,$code,'','');
                        if($customerID){
                            sendEmail('customerCode',array("to"=>$email,"code"=>$code));
                            $_SESSION['customer_email'] = $email;
                            $_SESSION['codeSent'] = true;
                            $_SESSION['code'] = $code;
                            $_SESSION['customerID'] = $customerID;
                            echo '{"status":"ok","msg":"email sent"}';

                        }
                    } #end if is customer
                    #header("Location:get_code.php");

                } else {
                    echo '{"status":"err","msg":"Lincoln Emails Only"}';
                }#end if lfg email
            }#end post['email'] check
        break;
    }
?>