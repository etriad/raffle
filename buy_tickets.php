<?php

    include_once("incConstants.php");
    $page = "buy_tickets";
    ob_start();
?>	
  <!--Main layout-->
  <main>
    <div class="container h-100">
        <h1>Buy Tickets</h1>
        <ul>
            <li>Buy Here</li>
            <li>Buy There</li>
        </ul>
    </div>
  </main>
  <!--Main layout-->

<?php
	$content = ob_get_clean();
	include_once('layout.php');
?>