<?php
include_once("incConstants.php");
secureAdmin();

  
  if(!isset($get['cid'])){
      header("location: admin_transactions.php");
  }

  $arrCustomer = getCustomer($customerID);
  $arrCustomerTickets = getCustomerTickets($customerID);
  $arrCustomerTicketTotal = getCustomerTicketTotal($customerID);
  $getCustomerTicketsUsed = getCustomerTicketsUsed($customerID);
  $getCustomeritem_tickets = getCustomeritem_tickets($customerID);

  #var_dump($arrCustomer);

  #var_dump($arrCustomerTickets);

  #var_dump($getCustomerTicketTotal);

  #var_dump($getCustomerTicketsUsed);

  #var_dump($getCustomeritem_tickets);

	
	ob_start();
?>
<h2>Customer: <?=$arrCustomer['customer_email']?> | <?=$arrCustomer['customer_code']?></h2>
<h3>Tickets Purchased: <?=$arrCustomerTicketTotal['total_cnt']?> | Tickets Used: <?=$getCustomerTicketsUsed?> | Total Purchase: $<?=$arrCustomerTicketTotal['total_amt']?></h3>
<hr>
<?php if(count($arrCustomerTickets)>0){ ?>
<table id="data" class="display" style="width:100%">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Qty</th>
      <th scope="col">$ Amount</th>
      <th scope="col">admin</th>
      <th scope="col">Timestamp</th>

    </tr>
  </thead>
  <tbody>
  <?php foreach($arrCustomerTickets as $transaction){ ?>
    <tr>
      <th scope="row"><?php echo $transaction['transaction_id']; ?></th>
      <td><?php echo $transaction['ticket_cnt']; ?></td>
      <td><?php echo $transaction['transaction_amt']; ?></td>
      <td><?php echo $transaction['admin_email']; ?></td>
      <td><?php echo $transaction['ticket_timestamp']; ?></td>
    </tr>
  <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Qty</th>
      <th scope="col">$ Amount</th>
      <th scope="col">admin</th>
      <th scope="col">Timestamp</th>

    </tr>
  </tfoot>
</table>

<script type="text/javascript" class="init">
	

    $(document).ready(function() {
        $('#data').DataTable();
    } );
    
    
</script>
<?php } ?>

<hr>


<?php if(count($getCustomeritem_tickets)>0){ ?>
<h3>Raffles Entered</h3>
<table id="item_tickets" class="display" style="width:100%">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Item</th>
      <th scope="col">Timestamp</th>

    </tr>
  </thead>
  <tbody>
  <?php foreach($getCustomeritem_tickets as $transaction){ ?>
    <tr>
      <th scope="row"><?php echo $transaction['transaction_id']; ?></th>
      <td><?php echo $transaction['item_name']; ?></td>
      <td><?php echo $transaction['timestamp']; ?></td>
    </tr>
  <?php } ?>
  </tbody>
  <tfoot>
    <tr>
    <th scope="col">ID</th>
      <th scope="col">Item</th>
      <th scope="col">Timestamp</th>

    </tr>
  </tfoot>
</table>

<script type="text/javascript" class="init">
	

    $(document).ready(function() {
        $('#item_tickets').DataTable();
    } );
    
    
</script>
<?php } else { ?>
<h4>No Raffle Entries Yet</h4>
<?php } ?>

<?php
	$content = ob_get_clean();
	include_once('adminLayout.php');
?>
