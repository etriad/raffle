<?php

	include_once("incConstants.php");

	$arrItems = getAllItems();
	$page = "items";
	
	ob_start();
?>
<div class="col-lg-10 offset-lg-1">

			<div class="row">
<?php foreach($arrItems as $item){ 
	$itemTotalTickets = (int)getItemTotalTickets($item['item_id']);
?>
				<div class="col-lg-4 col-sm-6 mb-4">
					<div class="card h-100 ">
						<img class="card-img-top" alt="<?php echo $item['item_name']; ?>" src="items/<?php echo $item['item_image']; ?>">
						<div class="card-body">
							<h5 class="card-title">
								<?php echo $item['item_name']; ?>
							</h5>
							<p>
								<?php 
									if($loggedIn){
										$cntCustomerItemTickets = (int)getCustomerItemTickets($_SESSION['customerID'],$item['item_id']);
										echo '<hr><a class="btn btn-primary" href="enter_drawing.php?itemID='.$item['item_id'].'">See &amp; Enter</a><hr>';
										if($cntCustomerItemTickets>0){ echo "You've entered ".$cntCustomerItemTickets." times."; }

									} else {
										echo '<a class="btn btn-outline-secondary" href="get_email.php">Register to Win</a>';
									}
								?>
							<h6><?php echo $itemTotalTickets; ?> Total Entries</h6>

							</p>
						</div>
					</div>
				</div>
<?php } ?>
			</div>
		</div>



<?php
	$content = ob_get_clean();
	include_once('mainLayout.php');
?>
