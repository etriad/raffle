<?php

    include_once("incConstants.php");
    $err = false;

    if(isset($_SESSION['codeSent'])){
        $err=true;
        $errMsg = "We emailed your access code.";
    }

    #if no email in session, send back to get email first
    if(!isset($_SESSION['customer_email'])){

        header("Location: get_email.php");

    } else {

        #if not an existing email, generate a code and email it
        if(!isset($_SESSION['customerID'])){
            $err = true;
            $errMsg = "Hi, you need to purchase tickets first. See a volunteer.";
        }
    }




    if(isset($post['code'])){
        $code = strtolower($post['code']);
        if(checkCustomerCode($_SESSION['customerID'],$code)){
            $_SESSION['loggedIn'] = true;
            header("Location:items.php");
        } else {
            $err = true;
            $errMsg = "That is not the correct code.";
        }
    }


	ob_start();
?>

        <div class="row">
            <div class="col-lg-8">
                <h3>Please enter your access code.</h3>
                <?php if($err){ echo '<p class="alert alert-danger">'.$errMsg.'</p>';} ?>
            <form role="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
				<div class="form-group">

					<label for="inputCode">
						Access Code
					</label>
					<input name="code" type="text" class="form-control" id="inputCode" required>
				</div>
				<button type="submit" class="btn btn-primary">
					Submit Your Access Code
				</button>
            </form>
            </div>
        </div>


<?php
	$content = ob_get_clean();
	include_once('mainLayout.php');
?>