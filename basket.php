<?php

	include_once("incConstants.php");

  include_once("incConstants.php");
  $err = false;

  #if no itemID or user is not logged in, back to items
  if(!isset($get['itemID']) || !$loggedIn){ 

      header("Location: items.php"); 

  } else {

  $itemID = $get['itemID'];
  $item = getItem($itemID);

  if(isset($post['itemID']) && $remainingTickets>0){
    $numtickets = $post['numtickets'];
    $i=1;
    while($i<=$numtickets){
      $transactionID = addItemTicket($_SESSION['customerID'],$post['itemID']);
      $i++;
    }
    header("Location:view_baskets.php");
  }

      $cntCustomerItemTickets = getCustomerItemTickets($_SESSION['customerID'],$itemID);
      if($totalTickets>0 && $remainingTickets<=0){
          $err = true;
          $errMsg = "You don't have any remaining tickets. Please purchase more to enter this drawing.";
      }

  }

	$page = "basket";
	
	ob_start();
?>


  <main>  <!--Main layout-->

    <div class="container h-100 align-middle">

			<div class="text-center">
			<?php #if($err){ echo "<h4>".$errMsg."</h4>";} ?>
			</div>
			<div class="row">
        <div class="col">
					<div class="card">
						<img class="card-img-top" alt="<?php echo $item['item_name']; ?>" src="items/<?php echo $item['item_image']; ?>">

					</div>
				</div>
				<div class="col mt-2">
					<h3><?=$item['item_name']?></h3>
					<p><?=$item['item_desc']?></p>
					<p>
								<?php 
									if($remainingTickets<=0){
										echo '<a class="btn btn-primary" href="buy_tickets.php">Purchase tickets to enter.</a>';
										echo '<a href="view_baskets.php" class="btn btn-default">Return to Items</a>';
									} else {
										?>
										<form role="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?itemID=<?php echo $itemID; ?>">
											<input type="hidden" name="itemID" value="<?php echo $itemID; ?>">
											<?php if($cntCustomerItemTickets>0){ echo '<h4>You have entered this raffle '.$cntCustomerItemTickets.' times.</h4>'; }?>

                        <hr>
                          <div class="form-group">
														<select class="form-control" id="numtickets" name="numtickets">
														<?php for($i=1;$i<=$remainingTickets;$i++){
															if($i==1){ $strTicket = "ticket"; } else { $strTicket = "tickets";}
															echo "<option value='$i'>$i $strTicket</option>";
														}
														?>
														</select>
													</div>

                        <hr>
                          <button type="submit" class="btn btn-lg btn-dark-green">Enter to Win!</button>
 

											<a href="view_baskets.php" class="btn btn-lg btn-outline-primary">Return to Baskets</a>
										</form>
									<?php
									}
								?>
							</p>

				</div>
			</div>


    </div>
  </main>
  <!--Main layout-->

<?php
	$content = ob_get_clean();
	include_once('layout.php');
?>