<?php

    include_once("incConstants.php");
    $err = false;
        if(isset($post['email'])){
            $email = strtolower($post['email']);
            if(preg_match('/@lfg\.com$/i',$email)){

                $arrCustomer = getCustomerByEmail($email);

                if($arrCustomer){
                    $customerID = $arrCustomer['customer_id'];
                    $_SESSION['customerID'] = $customerID;
                    $_SESSION['customer_email'] = $arrCustomer['customer_email'];
                } else {
                    $code = generateCode();
                    $customerID = addCustomer($email,$code,'','');
                    if(sendEmail('customerCode',array("to"=>$email,"code"=>$code))){
                        $_SESSION['customer_email'] = $email;
                        $_SESSION['codeSent'] = true;
                        $_SESSION['code'] = $code;
                    }
                } #end if is customer
                header("Location:get_code.php");

            } else {
                $err = true;
                $errMsg =  "Sorry, Only LFG email addresses allowed to participate.";
            }#end if lfg email
        }#end post['email'] check


	ob_start();
?>

    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h3 class="card-title text-center">Hi, please enter you LFG email address.</h3>
            <?php if($err){ echo "<h4>".$errMsg."</h4>";} ?>
            <form role="form" class="form-signin" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
              <div class="form-label-group">
                <label for="inputEmail">LFG Email address</label>
                <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required autofocus>
              </div>
              <button class="btn btn-lg btn-primary text-uppercase" type="submit">Sign in</button>
            </form>
          </div>
        </div>
      </div>
    </div>
<?php
	$content = ob_get_clean();
	include_once('mainLayout.php');
?>