<?php
	session_start();
	$sessionID = session_id();

	$ipAddress = $_SERVER['REMOTE_ADDR'];

	#enable show errors
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);

	$action='';
	$serverName = strtolower($_SERVER['SERVER_NAME']);
	$root = realpath($_SERVER["DOCUMENT_ROOT"]);

	#database connection
	$dsn = 'mysql:dbname=raffle;host=mysql';
	$dbuser = 'root';
	$dbpassword = 'tiger';

	try {
		$db = new PDO($dsn, $dbuser, $dbpassword, array(PDO::ATTR_PERSISTENT => true));
	} catch (PDOException $e) {
		echo "We are currently conduting maintenance. Please comeback soon!"; //user friendly message
		mail('etriad@gmail.com','DB Connection failure',$e);
	}	

	date_default_timezone_set('UTC');


	# need to write functions to sanitize them
	$get = $_GET;
	$post = $_POST;

	# company constants
	$ourConstants = array("url"=>"trophytransport.com",
	"contactEmail"=>"admin@trophytransport.com",
	"telephone"=>"877-644-9757",
	"address"=>"8510 Rosedale Drive",
	"cityStateZip"=>" Oak Ridge, NC 27310",
	"facebook"=>"https://www.facebook.com/trophytransport",
	"twitter"=>"https://twitter.com/MarketplacesLLC",
	"linkedin"=>"http://www.linkedin.com/company/3220834?trk=tyah",
	"googleplus"=>"https://plus.google.com/u/0/b/100497074590044632804/100497074590044632804/about");

	

	# pull common query variables
	$pageName = $_SERVER['PHP_SELF'];


	
	function unique_id($l = 8) {
		return substr(md5(uniqid(mt_rand(), true)), 0, $l);
	}

#	include_once($root."/mgmt/classes/class.phpmailer.php");


	# include functions that work with database
	#include_once($root."/inc/incDBFunctions.php");


	# include functions that generate emails
	#include_once($root."/inc/incEmailFunctions.php");


	#include session functions that manage login state and create/delete session data
	#require_once($root."/inc/incSession.php"); 


	if(isset($post['action'])){
		$action = $post['action'];
	} else {
		$action = ""; #set default
	}

	//if a signout request is made, go back home with a refresh

	if(isset($get['signoff']) && $get['signoff'] == 'Y') {
		header("Location: /home.php");
	}

	#define template variables 

	$css="";
	$javascript = "";
	$headerAdder = "";
	$bodyClass = "";
	$pageTitle = "Legal Bids"; # to be from database eventually
	$pageDesc = "Legal Bids Website";  # to be from database eventually
	$pageKeywords = "Legal Bids";  # to be from database eventually

	//adding things together in seconds

	$arrRequestTime = array('3 Days' => '259200', '7 Days' => '604800', '14 Days' => '1209600');

	date_default_timezone_set('America/New_York');
	
	
	function formatDate($datestr){
		$datetime = strtotime($datestr);
		$mysqldate = date("m/d/Y",$datetime);
		return $mysqldate;
	}


	for ($i = 0; $i < 10; $i++) {
		$arrCCYears[] = date('Y') + $i;
	}

	$arrCCMonths = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	
	$arrStates = array('Alabama'=>"AL", 'Alaska'=>"AK", 'Arizona'=>"AZ", 'Arkansas'=>"AR", 'California'=>"CA", 'Colorado'=>"CO", 'Connecticut'=>"CT", 'Delaware'=>"DE", 'District Of Columbia'=>"DC", 'Florida'=>"FL", 'Georgia'=>"GA", 'Hawaii'=>"HI", 'Idaho'=>"ID", 'Illinois'=>"IL", 'Indiana'=>"IN", 'Iowa'=>"IA", 'Kansas'=>"KS", 'Kentucky'=>"KY", 'Louisiana'=>"LA", 'Maine'=>"ME", 'Maryland'=>"MD", 'Massachusetts'=>"MA", 'Michigan'=>"MI", 'Minnesota'=>"MN", 'Mississippi'=>"MS", 'Missouri'=>"MO", 'Montana'=>"MT", 'Nebraska'=>"NE", 'Nevada'=>"NV", 'New Hampshire'=>"NH", 'New Jersey'=>"NJ", 'New Mexico'=>"NM", 'New York'=>"NY", 'North Carolina'=>"NC", 'North Dakota'=>"ND", 'Ohio'=>"OH", 'Oklahoma'=>"OK", 'Oregon'=>"OR", 'Pennsylvania'=>"PA", 'Rhode Island'=>"RI", 'South Carolina'=>"SC", 'South Dakota'=>"SD", 'Tennessee'=>"TN", 'Texas'=>"TX", 'Utah'=>"UT", 'Vermont'=>"VT", 'Virginia'=>"VA", 'Washington'=>"WA", 'West Virginia'=>"WV", 'Wisconsin'=>"WI", 'Wyoming'=>"WY",
    "British Columbia"=>"BC", 
    "Ontario"=>"ON", 
    "Newfoundland and Labrador"=>"NL", 
    "Nova Scotia"=>"NS", 
    "Prince Edward Island"=>"PE", 
    "New Brunswick"=>"NB", 
    "Quebec"=>"QC", 
    "Manitoba"=>"MB", 
    "Saskatchewan"=>"SK", 
    "Alberta"=>"AB", 
    "Northwest Territories"=>"NT", 
    "Nunavut"=>"NU",
    "Yukon Territory"=>"YT");
    
	$arrCountries = array("GB" => "United Kingdom","US" => "United States","AF" => "Afghanistan","AL" => "Albania","DZ" => "Algeria","AS" => "American Samoa","AD" => "Andorra","AO" => "Angola","AI" => "Anguilla","AQ" => "Antarctica","AG" => "Antigua And Barbuda","AR" => "Argentina","AM" => "Armenia","AW" => "Aruba","AU" => "Australia","AT" => "Austria","AZ" => "Azerbaijan","BS" => "Bahamas","BH" => "Bahrain","BD" => "Bangladesh","BB" => "Barbados","BY" => "Belarus","BE" => "Belgium","BZ" => "Belize","BJ" => "Benin","BM" => "Bermuda","BT" => "Bhutan","BO" => "Bolivia","BA" => "Bosnia And Herzegowina","BW" => "Botswana","BV" => "Bouvet Island","BR" => "Brazil","IO" => "British Indian Ocean Territory","BN" => "Brunei Darussalam","BG" => "Bulgaria","BF" => "Burkina Faso","BI" => "Burundi","KH" => "Cambodia","CM" => "Cameroon","CA" => "Canada","CV" => "Cape Verde","KY" => "Cayman Islands","CF" => "Central African Republic","TD" => "Chad","CL" => "Chile","CN" => "China","CX" => "Christmas Island","CC" => "Cocos (Keeling) Islands","CO" => "Colombia","KM" => "Comoros","CG" => "Congo","CD" => "Congo, The Democratic Republic Of The","CK" => "Cook Islands","CR" => "Costa Rica","CI" => "Cote D'Ivoire","HR" => "Croatia (Local Name: Hrvatska)","CU" => "Cuba","CY" => "Cyprus","CZ" => "Czech Republic","DK" => "Denmark","DJ" => "Djibouti","DM" => "Dominica","DO" => "Dominican Republic","TP" => "East Timor","EC" => "Ecuador","EG" => "Egypt","SV" => "El Salvador","GQ" => "Equatorial Guinea","ER" => "Eritrea","EE" => "Estonia","ET" => "Ethiopia","FK" => "Falkland Islands (Malvinas)","FO" => "Faroe Islands","FJ" => "Fiji","FI" => "Finland","FR" => "France","FX" => "France, Metropolitan","GF" => "French Guiana","PF" => "French Polynesia","TF" => "French Southern Territories","GA" => "Gabon","GM" => "Gambia","GE" => "Georgia","DE" => "Germany","GH" => "Ghana","GI" => "Gibraltar","GR" => "Greece","GL" => "Greenland","GD" => "Grenada","GP" => "Guadeloupe","GU" => "Guam","GT" => "Guatemala","GN" => "Guinea","GW" => "Guinea-Bissau","GY" => "Guyana","HT" => "Haiti","HM" => "Heard And Mc Donald Islands","VA" => "Holy See (Vatican City State)","HN" => "Honduras","HK" => "Hong Kong","HU" => "Hungary","IS" => "Iceland","IN" => "India","ID" => "Indonesia","IR" => "Iran (Islamic Republic Of)","IQ" => "Iraq","IE" => "Ireland","IL" => "Israel","IT" => "Italy","JM" => "Jamaica","JP" => "Japan","JO" => "Jordan","KZ" => "Kazakhstan","KE" => "Kenya","KI" => "Kiribati","KP" => "Korea, Democratic People's Republic Of","KR" => "Korea, Republic Of","KW" => "Kuwait","KG" => "Kyrgyzstan","LA" => "Lao People's Democratic Republic","LV" => "Latvia","LB" => "Lebanon","LS" => "Lesotho","LR" => "Liberia","LY" => "Libyan Arab Jamahiriya","LI" => "Liechtenstein","LT" => "Lithuania","LU" => "Luxembourg","MO" => "Macau","MK" => "Macedonia, Former Yugoslav Republic Of","MG" => "Madagascar","MW" => "Malawi","MY" => "Malaysia","MV" => "Maldives","ML" => "Mali","MT" => "Malta","MH" => "Marshall Islands","MQ" => "Martinique","MR" => "Mauritania","MU" => "Mauritius","YT" => "Mayotte","MX" => "Mexico","FM" => "Micronesia, Federated States Of","MD" => "Moldova, Republic Of","MC" => "Monaco","MN" => "Mongolia","MS" => "Montserrat","MA" => "Morocco","MZ" => "Mozambique","MM" => "Myanmar","NA" => "Namibia","NR" => "Nauru","NP" => "Nepal","NL" => "Netherlands","AN" => "Netherlands Antilles","NC" => "New Caledonia","NZ" => "New Zealand","NI" => "Nicaragua","NE" => "Niger","NG" => "Nigeria","NU" => "Niue","NF" => "Norfolk Island","MP" => "Northern Mariana Islands","NO" => "Norway","OM" => "Oman","PK" => "Pakistan","PW" => "Palau","PA" => "Panama","PG" => "Papua New Guinea","PY" => "Paraguay","PE" => "Peru","PH" => "Philippines","PN" => "Pitcairn","PL" => "Poland","PT" => "Portugal","PR" => "Puerto Rico","QA" => "Qatar","RE" => "Reunion","RO" => "Romania","RU" => "Russian Federation","RW" => "Rwanda","KN" => "Saint Kitts And Nevis","LC" => "Saint Lucia","VC" => "Saint Vincent And The Grenadines","WS" => "Samoa","SM" => "San Marino","ST" => "Sao Tome And Principe","SA" => "Saudi Arabia","SN" => "Senegal","SC" => "Seychelles","SL" => "Sierra Leone","SG" => "Singapore","SK" => "Slovakia (Slovak Republic)","SI" => "Slovenia","SB" => "Solomon Islands","SO" => "Somalia","ZA" => "South Africa","GS" => "South Georgia, South Sandwich Islands","ES" => "Spain","LK" => "Sri Lanka","SH" => "St. Helena","PM" => "St. Pierre And Miquelon","SD" => "Sudan","SR" => "Suriname","SJ" => "Svalbard And Jan Mayen Islands","SZ" => "Swaziland","SE" => "Sweden","CH" => "Switzerland","SY" => "Syrian Arab Republic","TW" => "Taiwan","TJ" => "Tajikistan","TZ" => "Tanzania, United Republic Of","TH" => "Thailand","TG" => "Togo","TK" => "Tokelau","TO" => "Tonga","TT" => "Trinidad And Tobago","TN" => "Tunisia","TR" => "Turkey","TM" => "Turkmenistan","TC" => "Turks And Caicos Islands","TV" => "Tuvalu","UG" => "Uganda","UA" => "Ukraine","AE" => "United Arab Emirates","UM" => "United States Minor Outlying Islands","UY" => "Uruguay","UZ" => "Uzbekistan","VU" => "Vanuatu","VE" => "Venezuela","VN" => "Viet Nam","VG" => "Virgin Islands (British)","VI" => "Virgin Islands (U.S.)","WF" => "Wallis And Futuna Islands","EH" => "Western Sahara","YE" => "Yemen","YU" => "Yugoslavia","ZM" => "Zambia","ZW" => "Zimbabwe");

?>
