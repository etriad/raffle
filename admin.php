<?php

    include_once("incConstants.php");
    $alert = false;

    if(isset($post['email']) && isset($post['code'])){
        $email = strtolower($post['email']);
        $code = strtolower($post['code']);
        if($arrAdmin = getAdmin($email,$code)){
            $_SESSION['admin_id'] = $arrAdmin['admin_id'];
            $_SESSION['admin_email'] = $arrAdmin['admin_email'];
            header("location:admin_buy_tickets.php");
        } else {
            $alert = true;
            $alertType = "danger";
            $alertMsg = "Invalid Login";
        }
    }
    ob_start();
?>	
<!-- Default form login -->
<main>
    <div class="container h-100">
        <div class="col-lg-6 offset-lg-3">
        <?php if($alert){ echo '<p class="alert alert-'.$alertType.'">'.$alertMsg.'</p>'; } ?>
        <form class="text-center border border-light p-5" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

            <p class="h4 mb-4">Admin Sign-In</p>

            <!-- Email -->
            <input type="email" name="email" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="E-mail">

            <!-- Password -->
            <input type="text" name="code" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="access code">


            <!-- Sign in button -->
            <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>


        </form>
        <!-- Default form login -->
        </div>
    </div>
</main>
<?php
	$content = ob_get_clean();
	include_once('layout.php');
?>