<?php
	include_once("inc/incConstants.php");
	include_once("shipmentModel.php");
	
	$userID = 1;
	

	$shipping_type = 'Economy';
	
	$shipper = array();
	$recipient = array();
	$billing = array();


	if(!empty($post)){

		$date_pickup = $post["date_pickup"];

		$domestic = $post["intl"];

		$shipper['company'] = $post["shipper_company"];
		$shipper['fname'] = $post["shipper_fname"];
		$shipper['lname'] = $post["shipper_lname"];
		$shipper['address1'] = $post["shipper_address1"];
		$shipper['address2'] = $post["shipper_address2"];
		$shipper['city'] = $post["shipper_city"];
		$shipper['state'] = $post["shipper_state"];
		$shipper['country'] = $post["shipper_country"];
		$shipper['zip'] = $post["shipper_zip"];
		$shipper['phone1'] = $post["shipper_phone1"];
		$shipper['phone2'] = $post["shipper_phone2"];
		$shipper['email'] = $post["shipper_email"];
		$shipper['fax'] = $post["shipper_fax"];
		$shipper['notes'] = $post["shipper_notes"];
		$shipper['update'] = $post["shipper_update"];
		$shipper['contact_id'] = $post["shipper_contact_id"];
		
		$recipient['company'] = $post["recipient_company"];
		$recipient['fname'] = $post["recipient_fname"];
		$recipient['lname'] = $post["recipient_lname"];
		$recipient['address1'] = $post["recipient_address1"];
		$recipient['address2'] = $post["recipient_address2"];
		$recipient['city'] = $post["recipient_city"];
		$recipient['state'] = $post["recipient_state"];
		$recipient['country'] = $post["recipient_country"];
		$recipient['zip'] = $post["recipient_zip"];
		$recipient['phone1'] = $post["recipient_phone1"];
		$recipient['phone2'] = $post["recipient_phone2"];
		$recipient['email'] = $post["recipient_email"];
		$recipient['fax'] = $post["recipient_fax"];
		$recipient['notes'] = $post["recipient_notes"];
		$recipient['update'] = $post["recipient_update"];
		$recipient['contact_id'] = $post["recipient_contact_id"];


		$bill_to = $post["bill_to"];
		
		switch ($bill_to){
			
			case 'third party':

				$billing['company'] = $post["billing_company"];
				$billing['fname'] = $post["billing_fname"];
				$billing['lname'] = $post["billing_lname"];
				$billing['address1'] = $post["billing_address1"];
				$billing['address2'] = $post["billing_address2"];
				$billing['city'] = $post["billing_city"];
				$billing['state'] = $post["billing_state"];
				$billing['country'] = $post["billing_country"];
				$billing['zip'] = $post["billing_zip"];
				$billing['phone1'] = $post["billing_phone1"];
				$billing['phone2'] = $post["billing_phone2"];
				$billing['email'] = $post["billing_email"];
				$billing['fax'] = $post["billing_fax"];
				$billing['notes'] = $post["billing_notes"];
				$billing['update'] = $post["billing_update"];
				$billing['contact_id'] = $post["billing_contact_id"];

			break;
		
			case 'shipper':
				$billing['company'] = $shipper['company'];
				$billing['fname'] = $shipper['fname'];
				$billing['lname'] = $shipper['lname'];
				$billing['address1'] = $shipper['address1'];
				$billing['address2'] = $shipper['address2'];
				$billing['city'] = $shipper['city'];
				$billing['state'] = $shipper['state'];
				$billing['country'] = $shipper['country'];
				$billing['zip'] = $shipper['zip'];
				$billing['phone1'] = $shipper['phone1'];
				$billing['phone2'] = $shipper['phone2'];
				$billing['email'] = $shipper['email'];
				$billing['fax'] = $shipper['fax'];
				$billing['notes'] = $shipper['notes'];
				$billing['update'] = $shipper['update'];
				$billing['contact_id'] = $shipper['contact_id'];
			break;
			
			case 'recipient':
				$billing['company'] = $recipient['company'];
				$billing['fname'] = $recipient['fname'];
				$billing['lname'] = $recipient['lname'];
				$billing['address1'] = $recipient['address1'];
				$billing['address2'] = $recipient['address2'];
				$billing['city'] = $recipient['city'];
				$billing['state'] = $recipient['state'];
				$billing['country'] = $recipient['country'];
				$billing['zip'] = $recipient['zip'];
				$billing['phone1'] = $recipient['phone1'];
				$billing['phone2'] = $recipient['phone2'];
				$billing['email'] = $recipient['email'];
				$billing['fax'] = $recipient['fax'];
				$billing['notes'] = $recipient['notes'];
				$billing['update'] = $recipient['update'];
				$billing['contact_id'] = $recipient['contact_id'];
			break;
			
		}
		
		$shipment_type = $post["shipment_type"];

		$arrPieceQuantity = $post["piece_quantity"];
		$arrPieceType = $post["piece_type"];
		$arrPieceAnimal = $post["piece_animal"];
		$arrPieceWeight = $post["piece_weight"];
		$arrPieceWidth = $post["piece_width"];
		$arrPieceLength = $post["piece_length"];
		$arrPieceHeight = $post["piece_height"];
		$arrPieceNotes = $post['piece_notes'];

		if($post['insurance']!='yes'){
			$insurance = 'no';
		} else {
			$insurance = 'yes';
		}

		$shipping_type = $post["shipping_type"];
		$insurance_value = $post["insurance_value"];
		$customs_value = $post["customs_value"];
		$charges_total = $post["charges_total"];
		
		$special_request = $post['special_request'];
				

		#$arrArgs = array($recipient['id,$billing['id,$billing['type,$shipping_type_id,$domestic,$insurance,$insurance_value,$customs_value,$intl_door,$intl_duties,$intl_broker,$intl_broker_name,$intl_broker_phone,$intl_broker_city,$intl_broker_country,$intl_broker_zip,$special_request,'Now()','active',$tracking_number,$date_pickup,$date_delivery,'null','null',$userID);


		$sql =	"INSERT INTO trophy_trophytransport.SHIPMENTSX
				(FROM_COMPANY, FROM_ADDRESS1, FROM_ADDRESS2, FROM_CITY, FROM_STATE, FROM_COUNTRY, FROM_ZIP, FROM_FNAME, FROM_LNAME, FROM_PHONE1, FROM_PHONE2, FROM_EMAIL, FROM_FAX, FROM_LAT, FROM_LONG, FROM_NOTES, TO_COMPANY, TO_ADDRESS1, TO_ADDRESS2, TO_CITY, TO_STATE, TO_COUNTRY, TO_ZIP, TO_FNAME, TO_LNAME, TO_PHONE1, TO_PHONE2, TO_EMAIL, TO_FAX, TO_LAT, TO_LONG, TO_NOTES, BILL_COMPANY, BILL_ADDRESS1, BILL_ADDRESS2, BILL_CITY, BILL_STATE, BILL_COUNTRY, BILL_ZIP, BILL_FNAME, BILL_LNAME, BILL_PHONE1, BILL_PHONE2, BILL_EMAIL, BILL_FAX, BILL_LAT, BILL_LONG, BILL_NOTES, BILL_TO, SHIP_TYPE, INSURANCE, INSURANCE_VALUE, CUSTOMS_VALUE, CHARGES_TOTAL, SPECIAL_REQUEST, DATE_PICKUP, MODIFY_DATE, STATUS) 
				VALUES (:from_company, :from_address1, :from_address2, :from_city, :from_state, :from_country, :from_zip, :from_fname, :from_lname, 
				:from_phone1, :from_phone2, :from_email, :from_fax, :from_lat, :from_long, :from_notes, :to_company, :to_address1, :to_address2, 
				:to_city, :to_state, :to_country, :to_zip, :to_fname, :to_lname, :to_phone1, :to_phone2, :to_email, :to_fax, :to_lat, :to_long, :to_notes, 
				:bill_company, :bill_address1, :bill_address2, :bill_city, :bill_state, :bill_country, :bill_zip, :bill_fname, :bill_lname, :bill_phone1, 
				:bill_phone2, :bill_email, :bill_fax, :bill_lat, :bill_long, :bill_notes, :bill_to, :ship_type, :insurance, :insurance_value, 
				:customs_value, :charges_total, :special_request, :date_pickup, 'Now()', 'active')";
				
		$query = $db->prepare($sql);
		$query->bindValue(':from_company',$shipper['company']);
		$query->bindValue(':from_address1',$shipper['address1']);
		$query->bindValue(':from_address2',$shipper['address2']);
		$query->bindValue(':from_city',$shipper['city']);
		$query->bindValue(':from_state',$shipper['state']);
		$query->bindValue(':from_country',$shipper['country']);
		$query->bindValue(':from_zip',$shipper['zip']);
		$query->bindValue(':from_fname',$shipper['fname']);
		$query->bindValue(':from_lname',$shipper['lname']);
		$query->bindValue(':from_phone1',$shipper['phone1']);
		$query->bindValue(':from_phone2',$shipper['phone2']);
		$query->bindValue(':from_email',$shipper['email']);
		$query->bindValue(':from_fax',$shipper['fax']);
		$query->bindValue(':from_lat',$shipper['lat']);
		$query->bindValue(':from_long',$shipper['long']);
		$query->bindValue(':from_notes',$shipper['notes']);
		$query->bindValue(':to_company',$recipient['company']);
		$query->bindValue(':to_address1',$recipient['address1']);
		$query->bindValue(':to_address2',$recipient['address2']);
		$query->bindValue(':to_city',$recipient['city']);
		$query->bindValue(':to_state',$recipient['state']);
		$query->bindValue(':to_country',$recipient['country']);
		$query->bindValue(':to_zip',$recipient['zip']);
		$query->bindValue(':to_fname',$recipient['fname']);
		$query->bindValue(':to_lname',$recipient['lname']);
		$query->bindValue(':to_phone1',$recipient['phone1']);
		$query->bindValue(':to_phone2',$recipient['phone2']);
		$query->bindValue(':to_email',$recipient['email']);
		$query->bindValue(':to_fax',$recipient['fax']);
		$query->bindValue(':to_lat',$recipient['lat']);
		$query->bindValue(':to_long',$recipient['long']);
		$query->bindValue(':to_notes',$recipient['notes']);
		$query->bindValue(':bill_company',$billing['company']);
		$query->bindValue(':bill_address1',$billing['address1']);
		$query->bindValue(':bill_address2',$billing['address2']);
		$query->bindValue(':bill_city',$billing['city']);
		$query->bindValue(':bill_state',$billing['state']);
		$query->bindValue(':bill_country',$billing['country']);
		$query->bindValue(':bill_zip',$billing['zip']);
		$query->bindValue(':bill_fname',$billing['fname']);
		$query->bindValue(':bill_lname',$billing['lname']);
		$query->bindValue(':bill_phone1',$billing['phone1']);
		$query->bindValue(':bill_phone2',$billing['phone2']);
		$query->bindValue(':bill_email',$billing['email']);
		$query->bindValue(':bill_fax',$billing['fax']);
		$query->bindValue(':bill_lat',$billing['lat']);
		$query->bindValue(':bill_long',$billing['long']);
		$query->bindValue(':bill_notes',$billing['notes']);
		$query->bindValue(':bill_to',$bill_to);
		$query->bindValue(':ship_type',$shipping_type);
		$query->bindValue(':insurance',$insurance);
		$query->bindValue(':insurance_value',$insurance_value);
		$query->bindValue(':customs_value',$customs_value);
		$query->bindValue(':charges_total',$charges_total);
		$query->bindValue(':special_request',$special_request);
		$query->bindValue(':date_pickup',date('Y-m-d',strtotime($date_pickup)));
		
		try { 
			$query->execute(); 
			$shipmentID =  $db->lastInsertId(); 
		} catch(PDOExecption $e) { 
			$db->rollback(); 
			print "Error!: " . $e->getMessage() . "</br>"; 
		} 		
		
		
		$sql = "UPDATE SHIPMENTSX SET TRACKING_NUMBER = :tracking_number WHERE SHIPMENT_ID = :shipment_id";
		$querytrack = $db->prepare($sql);
		$querytrack->bindValue(':tracking_number','TT-'.$shipper['state'].$recipient['state'].'-'.date('mdy',strtotime($date_pickup)).'-'.$shipmentID);
		$querytrack->bindValue(':shipment_id',$shipmentID);

		try { 
			$querytrack->execute(); 
		} catch(PDOExecption $e) { 
			$db->rollback(); 
			print "Error!: " . $e->getMessage() . "</br>"; 
		} 		
	
		$shipper['type'] = 'shipper';
		$recipient['type'] = 'recipient';
		$billing['type'] = 'billing';
		
		$arrParties = array($shipper,$recipient,$billing);
		
		foreach($arrParties as $party){
	
			if($party['type']=='billing'){
				$contact_type = 2;
			} else {
				$contact_type = 1;
			}
			
			if($party['contact_id']!='0' && $party['update']=='y'){
	
				$sql = "UPDATE CONTACTS 
				set 
				CONTACT_TYPE = :contact_type,
				CONTACT_FNAME = :contact_fname, 
				CONTACT_LNAME = :contact_lname, 
				CONTACT_COMPANY = :contact_company, 
				CONTACT_ADDRESS1 = :contact_address1, 
				CONTACT_ADDRESS2 = :contact_address2, 
				CONTACT_CITY = :contact_city, 
				CONTACT_STATE = :contact_state, 
				CONTACT_COUNTRY = :contact_country, 
				CONTACT_ZIP = :contact_zip, 
				CONTACT_PHONE = :contact_phone, 
				CONTACT_ALT_PHONE = :contact_alt_phone, 
				CONTACT_FAX = :contact_fax, 
				CONTACT_EMAIL = :contact_email, 
				CONTACT_STATUS = :contact_status 
				WHERE CONTACT_ID = :contact_id";
				
			
			
				$query2 = $db->prepare($sql);
				$query2->bindValue(':contact_type',$contact_type);
				$query2->bindValue(':contact_fname',$party['fname']);
				$query2->bindValue(':contact_lname',$party['lname']);
				$query2->bindValue(':contact_company',$party['company']);
				$query2->bindValue(':contact_address1',$party['address1']);
				$query2->bindValue(':contact_address2',$party['address2']);
				$query2->bindValue(':contact_city',$party['city']);
				$query2->bindValue(':contact_state',$party['state']);
				$query2->bindValue(':contact_country',$party['country']);
				$query2->bindValue(':contact_zip',$party['zip']);
				$query2->bindValue(':contact_phone',$party['phone1']);
				$query2->bindValue(':contact_alt_phone',$party['phone2']);
				$query2->bindValue(':contact_fax',$party['fax']);
				$query2->bindValue(':contact_email',$party['email']);
				$query2->bindValue(':contact_status','active');
				$query2->bindValue(':contact_id',$party['contact_id']);
		
				try { 
					$query2->execute(); 
				} catch(PDOExecption $e) { 
					$db->rollback(); 
					print "Error!: " . $e->getMessage() . "</br>"; 
				} 		
	
			} else if(empty($party['contact_id'])){
				
				
				$sql = "INSERT INTO CONTACTS
				(CONTACT_TYPE,CONTACT_FNAME, CONTACT_LNAME, CONTACT_COMPANY, 
				CONTACT_ADDRESS1, CONTACT_ADDRESS2, CONTACT_CITY, CONTACT_STATE, CONTACT_COUNTRY, CONTACT_ZIP, 
				CONTACT_PHONE, CONTACT_ALT_PHONE, CONTACT_FAX, CONTACT_EMAIL,
				CONTACT_STATUS, CONTACT_LAT, CONTACT_LONG) 
				values (
				:contact_type,
				:contact_fname, 
				:contact_lname, 
				:contact_company, 
				:contact_address1, 
				:contact_address2, 
				:contact_city, 
				:contact_state, 
				:contact_country, 
				:contact_zip, 
				:contact_phone, 
				:contact_alt_phone, 
				:contact_fax, 
				:contact_email, 
				'active', 
				:contact_latitude, 
				:contact_longitude)";
				
				$query3 = $db->prepare($sql);
				$query3->bindValue(':contact_type',$contact_type);
				$query3->bindValue(':contact_fname',$party['fname']);
				$query3->bindValue(':contact_lname',$party['lname']);
				$query3->bindValue(':contact_company',$party['company']);
				$query3->bindValue(':contact_address1',$party['address1']);
				$query3->bindValue(':contact_address2',$party['address2']);
				$query3->bindValue(':contact_city',$party['city']);
				$query3->bindValue(':contact_state',$party['state']);
				$query3->bindValue(':contact_country',$party['country']);
				$query3->bindValue(':contact_zip',$party['zip']);
				$query3->bindValue(':contact_phone',$party['phone1']);
				$query3->bindValue(':contact_alt_phone',$party['phone2']);
				$query3->bindValue(':contact_fax',$party['fax']);
				$query3->bindValue(':contact_email',$party['email']);
				$query3->bindValue(':contact_latitude',$party['latitude']);
				$query3->bindValue(':contact_longitude',$party['longitude']);
				
				try { 
					$query3->execute(); 
				} catch(PDOExecption $e) { 
					$db->rollback(); 
					print "Error!: " . $e->getMessage() . "</br>"; 
				} 		
				
			} #end if
			
		} #end foreach
		
		if(!empty($arrPieceQuantity) and count($arrPieceQuantity)>0){
			$sql = "INSERT INTO SHIPMENT_PIECES (SHIPMENT_ID,PIECE_TYPE_ID,PIECE_ANIMAL_ID,PIECE_QTY,PIECE_WEIGHT,PIECE_WIDTH,PIECE_LENGTH,PIECE_HEIGHT,PIECE_NOTES)
					VALUES (:shipment_id,:piece_type_id,:piece_animal_id,:piece_qty,:piece_weight,:piece_width,:piece_length,:piece_height,:piece_notes)";
			$queryPiece = $db->prepare($sql);
			foreach($arrPieceQuantity as $key=>$val){
			
				$queryPiece->bindValue(':shipment_id',$shipmentID);
				$queryPiece->bindValue(':piece_type_id',$arrPieceType[$key]);
				$queryPiece->bindValue(':piece_animal_id',$arrPieceAnimal[$key]);
				$queryPiece->bindValue(':piece_qty',$arrPieceQuantity[$key]);
				$queryPiece->bindValue(':piece_weight',$arrPieceWeight[$key]);
				$queryPiece->bindValue(':piece_width',$arrPieceWidth[$key]);
				$queryPiece->bindValue(':piece_length',$arrPieceLength[$key]);
				$queryPiece->bindValue(':piece_height',$arrPieceHeight[$key]);
				$queryPiece->bindValue(':piece_notes',$arrPieceNotes[$key]);
				
				$queryPiece->execute();
				
			}#end foreach
		}#end empty and count check
	
		header("Location:shipment_list.php?status=active");
	
	} #end if posted
	
	
	ob_start();
?>
	<div class="page-header">
    	<h1>New Master Bill</h1>
    </div>

	<form id="frmShipment" method="post">
    	<input type="hidden" id="shipper_contact_id" name="shipper_contact_id" value="0" />
        <input type="hidden" id="recipient_contact_id" name="recipient_contact_id" value="0" />
        <input type="hidden" id="billing_contact_id" name="billing_contact_id" value="0" />
        <input type="hidden" id="domestic" name="domestic" value="y" />
        <input type="hidden" id="billing_lat" name="billing_lat" value="" />
        <input type="hidden" id="shipper_lat" name="shipper_lat" value="" />
        <input type="hidden" id="recipient_lat" name="recipient_lat" value="" />
        <input type="hidden" id="billing_long" name="billing_long" value="" />
        <input type="hidden" id="shipper_long" name="shipper_long" value="" />
        <input type="hidden" id="recipient_long" name="recipient_long" value="" />

  <div class="row" id="block_billing">
    <div class="col-md-6">
      <div class="row">
        <fieldset>
          <legend>Billing:</legend>
 		  <div class="row">
            <div class="col-sm-10">
              <div class="form-group">
                <input type="search" class="form-control input-sm contact_search" id="billing_contact" name="shipper_contact" placeholder="search contact">
              </div>
            </div>
          </div>


          <div class="row contact_update" id="billing_update_row">
            <div class="col-sm-10">
                <div class="checkbox">
                    <label>
                    <input type="checkbox" name="billing_update" value="y"> Update
                    </label>
                </div>
            </div>
          </div>
          <!--/.row -->


          <div class="row">
            <div class="col-sm-10">
              <div class="form-group">
                <input type="search" class="form-control input-sm geocomplete" id="billing_geocomplete" name="billing_geocomplete" placeholder="address lookup">
              </div>
            </div>
          </div>
          <!--/.row -->

          <div class="row">
            <div class="col-sm-10">
              <div class="form-group">
                <input type="text" class="form-control input-sm" id="billing_company" name="billing_company" placeholder="business name">
              </div>
            </div>
          </div>
          <!--/.row -->

         <div class="row">
          	<div class="col-sm-1">Attn:</div>
            <div class="col-sm-4">
              <div class="form-group">
                <input type="text" class="form-control input-sm" id="billing_fname" name="billing_fname" placeholder="first name">
              </div>
            </div>
            <div class="col-sm-5">
              <div class="form-group">
                <input type="text" class="form-control input-sm" id="billing_lname" name="billing_lname"  placeholder="last name">
              </div>
            </div>
          </div>
          <!-- /.row -->
          
          <div class="row">
            <div class="col-sm-10">
              <div class="form-group">
                <input type="text" class="form-control input-sm" id="billing_address1" name="billing_address1" placeholder="address1">
              </div>
            </div>
          </div>
          <!--/.row -->
          
          <div class="row">
            <div class="col-sm-10">
              <div class="form-group">
                <input type="text" class="form-control input-sm" id="billing_address2" name="billing_address2" placeholder="address2">
              </div>
            </div>
          </div>
          <!-- /.row -->
          
          <div class="row">
            <div class="col-sm-5">
              <div class="form-group">
                <input type="text" class="form-control input-sm" id="billing_city" name="billing_city" placeholder="city">
              </div>
            </div>
            <div class="col-sm-5">
              <div class="form-group">
              <select class="form-control input-sm"  name="billing_state" id="billing_state" >
                <option value="">Select State</option>
                  <?php
					foreach($arrStates as $key=>$val){
					?>
                  <option value="<?=$val?>" <?php if($val==$billing['state']){ echo 'selected="selected"'; }?>>
                  <?=$key?>
                  </option>
                  <?php } #end country loop ?>
              </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-5">
              <div class="form-group">
              <select class="form-control input-sm"  name="billing_country" id="billing_country" >
                <option value="">Select Country</option>
                <option value="US">United States</option>
                  <?php
					foreach($arrCountries as $key=>$val){
					?>
                  <option value="<?=$key?>" <?php if($key==$billing['country']){ echo 'selected="selected"'; }?>>
                  <?=$val?>
                  </option>
                  <?php } #end country loop ?>
              </select>
              </div>
            </div>
            <div class="col-sm-5">
              <div class="form-group">
                <input type="text"  class="form-control input-sm" id="billing_zip" name="billing_zip" placeholder="postal code">
              </div>
            </div>
          </div>
          <!--/.row-->


          <div class="row">
            <div class="col-sm-5">
              <div class="form-group">
                <input type="text" class="form-control input-sm fphone" id="billing_phone1" name="billing_phone1" placeholder="Phone #" value="<?=$billing['phone1']?>">
              </div>
            </div>
            <div class="col-sm-5">
              <div class="form-group">
                <input type="text" class="form-control input-sm fphone" id="billing_phone2" name="billing_phone2" placeholder="Alt. Phone #" value="<?=$billing['phone2']?>">
              </div>
            </div>
          </div>
          <!--/.row-->   
          
          <div class="row">
            <div class="col-sm-5">
              <div class="form-group">
                <input type="email" class="form-control input-sm" id="billing_email" name="billing_email" placeholder="Email" value="<?=$billing['email']?>">
              </div>
            </div>
            <div class="col-sm-5">
              <div class="form-group">
                <input type="text" class="form-control input-sm fphone" id="billing_fax" name="billing_fax" placeholder="Fax#" value="<?=$billing['fax']?>">
              </div>
            </div>
          </div>
          <!--/.row-->          
          
          <div class="row">
          	<div class="col-sm-10">
            	<div class="form-group">
                <textarea type="text" class="form-control input-sm" rows="2" id="billing_notes" name="billing_notes" placeholder="billing notes"><?=$billing['notes']?></textarea>
                </div>
            </div>
          </div><!--/.row-->

          
        </fieldset>
      </div>
      <!--./col-sm-6--> 
    </div>
    <!-- /.row --> 
  </div>
  <!--/#block_billing--> 
 
 
 
 
        
        <div class="row" id="block_special_request">
            <fieldset>
                <legend>Notes:</legend>
                <textarea rows="3" class="col-sm-8" id="special_request" name="special_request" placeholder="Special Service Request"></textarea>
            </fieldset>
        </div><!--/#block_special_request -->

 
 
 		<div class="row">
        	<button type="submit" class="btn btn-success btn-lg">Submit</button>
        </div>
  
  </form>
  
  <style>
  	#block_intl_services .row {
		margin-top:3px;
	}
	.geocomplete,.contact_search {
		background-color:#efefef;
		padding-left:55px;
		border:1px solid #f5f5f5;
		font-size:13px;color:gray;
		background-image:url('http://i47.tinypic.com/r02vbq.png');
		background-repeat:no-repeat;
		background-position:right center;outline:0;
	}
	.piece {
		margin-bottom:10px;
	}
  </style>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <script src="assets/js/jquery.geocomplete.js"></script>
  
  <script>
    $(function() {
		
	$(".fphone").mask("(999) 999-9999");

	Object.size = function(obj) {
		var size = 0, key;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	};

	
	$(".geocomplete")
	.geocomplete()
	.bind("geocode:result", function(even, result){
		
		var arrAddress = result.address_components;
		$.each( arrAddress, function( key, value ) {
			console.log(value.types[0]);
			console.log(value.short_name);
		  if(value.types[0]=='street_number'){ street_number = value.short_name; }
		  if(value.types[0]=='route'){ route = value.short_name; }
		  if(value.types[0]=='locality'){ city = value.short_name; }
		  if(value.types[0]=='administrative_area_level_1'){ state = value.short_name; }
		  if(value.types[0]=='country'){ country = value.short_name; }
		  if(value.types[0]=='zip'){ postal_code = value.short_name; }
		});
		latitude = result.geometry.location.lat();
		longitude = result.geometry.location.lng();
		console.log(latitude);
		console.log(longitude);
		thisID = $(this).attr("id").split('_');
		contactType = thisID[0];
		$("#"+contactType+"_address1").val(street_number+' '+route);
		$("#"+contactType+"_city").val(city);
		$("#"+contactType+"_state option[value='"+state+"']").attr("selected", "selected");
		$("#"+contactType+"_zip").val(postal_code);
		$("#"+contactType+"_country option[value='"+country+"']").attr("selected", "selected");
		$("#"+contactType+"_lat").val(latitude);
		$("#"+contactType+"_long").val(longitude);
	});

	$("#recipient_geocomplete")
	.geocomplete()
	.bind("geocode:result", function(even, result){
		console.log(result);
		var arrAddress = result.address_components;
		$("#recipient_address1").val(arrAddress[0].long_name+' '+arrAddress[1].long_name);
		$("#recipient_city").val(arrAddress[3].long_name);
		$("#recipient_state option[value='"+arrAddress[5].short_name+"']").attr("selected", "selected");
		$("#recipient_zip").val(arrAddress[7].long_name);
		$("#recipient_country option[value='"+arrAddress[6].short_name+"']").attr("selected", "selected");
	});

	$("#billing_geocomplete")
	.geocomplete()
	.bind("geocode:result", function(even, result){
		console.log(result);
		var arrAddress = result.address_components;
		$("#billing_address1").val(arrAddress[0].long_name+' '+arrAddress[1].long_name);
		$("#billing_city").val(arrAddress[3].long_name);
		$("#billing_state option[value='"+arrAddress[5].short_name+"']").attr("selected", "selected");
		$("#billing_zip").val(arrAddress[7].long_name);
		$("#billing_country option[value='"+arrAddress[6].short_name+"']").attr("selected", "selected");
	});



///////////////////////////////////////////
	
	$( ".datepicker" ).datepicker({
      numberOfMonths: 2,
      showButtonPanel: true
    });		
	
///////////////////////////////////////////

	$('.contact_update').hide();
	

///////////////////////////////////////////
	
	$("input[name=bill_to]").on('change',function(){
		var billType = $(this).val();
		if(billType=='third party'){
			$("#block_billing").show('slow');
		} else {
			$("#block_billing").hide('slow');
		}
	});
		
///////////////////////////////////////////
/*
	$('#insurance_value').hide();
	$('#customs_value').hide();
	$("input[name=insurance]").on('change',function(){
		if($(this).attr('checked')=='checked'){
			$('#insurance_value').show('fast');
			$('#customs_value').show('fast');
		} else {
			$('#insurance_value').hide('fast');
			$('#customs_value').hide('fast');
		}
	});
*/
///////////////////////////////////////////

      var cache = {};
      $( ".contact_search" ).autocomplete({
        minLength: 2,
        source: function( request, response ) {
          var term = request.term;
          if ( term in cache ) {
            response( cache[ term ] );
            return;
          }
   
          $.getJSON( "ajaxFunctions.php?fn=getContacts", request, function( data, status, xhr ) {
            cache[ term ] = data;
            response( data );
          });
        }
      });
    });
    
    $( ".contact_search" ).on( "autocompleteselect", function( event, ui ) {
          $(this).val('');
          $arrType = $(this).attr('id').split('_');
          $type = $arrType[0];
          $("#groupList").empty();
          var contactID = ui.item.id;
          $.getJSON( "ajaxFunctions.php",{fn:"getContact" , id:contactID } ) 
          .done(function( data ){
			  $("#"+$type+"_update_row").show('fast');
			  $("#"+$type+"_contact_id").val(data.CONTACT_ID);
              $("#"+$type+"_fname").val(data.CONTACT_FNAME);
              $("#"+$type+"_lname").val(data.CONTACT_LNAME);
              $("#"+$type+"_company").val(data.CONTACT_COMPANY);
              $("#"+$type+"_address1").val(data.CONTACT_ADDRESS1);
              $("#"+$type+"_address2").val(data.CONTACT_ADDRESS2);
              $("#"+$type+"_city").val(data.CONTACT_CITY);
			  $("#"+$type+"_state option[value='"+data.CONTACT_STATE+"']").attr("selected", "selected");
              $("#"+$type+"_zip").val(data.CONTACT_ZIP);
              $("#"+$type+"_phone1").val(data.CONTACT_PHONE);
              $("#"+$type+"_phone2").val(data.CONTACT_ALT_PHONE);
			  $("#"+$type+"_email").val(data.CONTACT_EMAIL);
			  $("#"+$type+"_fax").val(data.CONTACT_FAX);
			  $("#"+$type+"_country option[value='"+data.CONTACT_COUNTRY+"']").attr("selected", "selected");
          });
      } );
	  
///////////////////////////////////////////

		updateTotals = function(){
		
			var weightsum = 0;
			var qtysum = 0;
			var qty = 1;
			
			$('.piece_weight').each(function() {
				var i = $('.piece_weight').index($(this));
				var arrQty = $(".piece_quantity");
				qty = arrQty[i].value;
				weightsum += Number($(this).val())*qty;
			});	
			
			$('.piece_quantity').each(function() {
				qty = Number($(this).val());
				qtysum += qty;
			});	
			
			$("#pieceWeightTotals").html("Total Weight: "+weightsum+" lbs.");
			$("#pieceNumTotal").html("Total Qty: "+qtysum+" pcs.");
		
		}
		
		$("body").on('change','.piece_weight,.piece_quantity',function(e){
			updateTotals();
		});
		

		 $("#addPiece").on('click', function(e) {
			e.preventDefault();
			$('#pieces_container').append(clonePiece.clone());
			updateTotals();
		});			
		

		$("body").on('click','span.piece_remove',function(e){
			e.preventDefault();
			$(this).closest("div.piece").remove();
			updateTotals();
		});

	 	var	clonePiece = $("div.piece").clone();

    </script>
  
<?php
	$content = ob_get_clean();
	include_once('mainLayout.html.php');
?>