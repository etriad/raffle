<?php

  include_once("incConstants.php");
  secureAdmin();

  $arrTransactions = getAllCustomerTickets();
  $arrStats = getCustomerTicketStats();

	
	ob_start();
?>
<h2>Tickets Purchased</h2>
<h3>Tickets Sold: <?=$arrStats['total_tickets']?> | Total Received: $<?=$arrStats['total_amt']?> | # of Customers: <?=$arrStats['num_customers']?></h3>
<table id="data" class="display" style="width:100%">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Email</th>
      <th scope="col">Qty</th>
      <th scope="col">$ Amount</th>
      <th scope="col">admin</th>
      <th scope="col">Timestamp</th>

    </tr>
  </thead>
  <tbody>
  <?php foreach($arrTransactions as $transaction){ ?>
    <tr>
      <th scope="row"><?php echo $transaction['transaction_id']; ?></th>
      <td><a href="admin_customer_detail.php?cid=<?php echo $transaction['customer_id'];?>"><?php echo $transaction['customer_email']; ?></a></td>
      <td><?php echo $transaction['ticket_cnt']; ?></td>
      <td><?php echo $transaction['transaction_amt']; ?></td>
      <td><?php echo $transaction['admin_email']; ?></td>
      <td><?php echo $transaction['ticket_timestamp']; ?></td>
    </tr>
  <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Email</th>
      <th scope="col">Qty</th>
      <th scope="col">$ Amount</th>
      <th scope="col">admin</th>
      <th scope="col">Timestamp</th>

    </tr>
  </tfoot>
</table>

<script type="text/javascript" class="init">
	

    $(document).ready(function() {
        $('#data').DataTable();
    } );
    
    
</script>

<?php
	$content = ob_get_clean();
	include_once('adminLayout.php');
?>
