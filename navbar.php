  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container">

      <!-- Brand -->
      <a class="navbar-brand" href="#">
        <img src="assets/lfg_uw_mini_banner.png" height="30" alt="Lincoln Financial &amp; United Way">
      </a>

      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- Left -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link waves-effect" href="home.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link waves-effect" href="view_baskets.php">Baskets</a>
          </li>
          <li class="nav-item">
            <a class="nav-link waves-effect" href="buy.php" target="_blank">Buy Tickets</a>
          </li>
          <li class="nav-item">
            <a class="nav-link waves-effect" href="about.php" target="_blank">About the Raffle</a>
          </li>
        </ul>

        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
        <?php if ($totalTickets > 0 && $remainingTickets > 0) { ?>
          <li class="nav-item">
            <a class="nav-link waves-effect">
              <span class="badge red z-depth-1 mr-1"> <?=$remainingTickets?> </span>
              <i class="fa fa-ticket"></i>
              <span class="clearfix d-none d-sm-inline-block"> Tickets </span>
            </a>
          </li>
        <?php } ?>
          <?php if($loggedIn){ ?>
          <li class="nav-item">
            <a href="logout.php" class="nav-link waves-effect" target="_blank">
              Log Off
            </a>
          </li>
          <?php } ?>


        </ul>

      </div>

    </div>
  </nav>
  <!-- Navbar -->
