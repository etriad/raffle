<?php
    include_once("incConstants.php");
    secureAdmin();
    $success = false;
    $err = false;


        if(isset($post['submitted'])){
         #form posted, check if a valid extension and existing email
           $email = strtolower($post['email']);
            if(preg_match('/@lfg\.com$/i',$email) || preg_match('/@lfd\.com$/i',$email)){

                $arrCustomer = getCustomerByEmail($email);

                if($arrCustomer){
                    $customerID = $arrCustomer['customer_id'];
                    $customerCode = $arrCustomer['customer_code'];
                } else {
                    $customerCode = generateCode();
                    $customerID = addCustomer($email,$customerCode,'','');
                    if(sendEmail('customerCode',array("to"=>$email,"code"=>$customerCode))){
                        $_SESSION['codeSent'] = true;
                        $_SESSION['code'] = $customerCode;
                    }
                } #end if is customer

                $qty = (int)$post['qty'];
                $amt = (int)$post['amt'];

                $transactionID = addCustomerTicket($customerID,$email,$qty,$amt,$_SESSION['admin_id']);

                $success = true;

            } else {
                $err = true;
                $errMsg =  "Sorry, Only LFG email addresses allowed to participate.";
            }#end if lfg email
        }#end post['email'] check


	ob_start();
?>

            <div class="row">
            <div class="col-xs-12 col-lg-4">
            <h3>Admin - Purchase Tickets</h3>
            <p>This form is used to assign tickets to a customer upon payment. Just enter their Lincoln email, the # of tickets purchased, and the amount ($usd) collected. They will receive a confirmation email.</p>
            <?php if($success==true){ ?>
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Success!</strong> <?php echo $qty." tickets where applied to ".$email." account. Transaction ID: ".$transactionID; ?>
            </div>
            <?php } ?>
            <?php if($err==true){ ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Oops!</strong> <?=$errMsg?>
            </div>
            <?php } ?>

            <form role="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <input type="hidden" name="submitted" value="true">
				<div class="form-group">

					<label for="inputEmail">
						Email address
					</label>
					<input name="email" type="email" class="form-control" id="inputEmail" required>
                </div>
                <div class="form-group">
                    <label for="inputQty">Qty of Tickets</label>
                    <input type="number" class="form-control" id="inputQty" name="qty" placeholder="qty" required>
                </div>
                <div class="form-group">
                    <label for="inputAmount">Payment Amount ($usd)</label>
                    <input type="number" class="form-control" id="inputAmount" name="amt" placeholder="$ Amount" required>
                </div>
				<button type="submit" class="btn btn-primary">
					Submit
				</button>
            </form>
            </div>
            </div>

<?php
	$content = ob_get_clean();
	include_once('adminLayout.php');
?>