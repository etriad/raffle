-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql:3306
-- Generation Time: Oct 08, 2018 at 04:24 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `raffle`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(20) NOT NULL,
  `admin_lname` varchar(20) NOT NULL,
  `admin_email` varchar(50) NOT NULL,
  `admin_code` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_fname`, `admin_lname`, `admin_email`, `admin_code`) VALUES
(1, 'phillip', 'planes', 'phil@lfg.com', 'phil');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `customer_code` varchar(5) NOT NULL,
  `customer_fname` varchar(20) NOT NULL,
  `customer_lname` varchar(20) NOT NULL,
  `location_id` int(11) NOT NULL,
  `customer_confirmed` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_email`, `customer_code`, `customer_fname`, `customer_lname`, `location_id`, `customer_confirmed`) VALUES
(1, 'phillip.planes@lfg.com', '7803', '', '', 1, 0),
(2, 'test@lfd.com', '2beb', '', '', 1, 0),
(3, 'john.kelly@lfg.com', '627c', '', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_tickets`
--

CREATE TABLE `customer_tickets` (
  `transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ticket_cnt` int(11) NOT NULL,
  `transaction_amt` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `ticket_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_tickets`
--

INSERT INTO `customer_tickets` (`transaction_id`, `customer_id`, `ticket_cnt`, `transaction_amt`, `admin_id`, `ticket_timestamp`) VALUES
(1, 1, 10, 10, 1, '2018-10-08 15:45:45'),
(2, 2, 10, 12, 1, '2018-10-08 16:00:43'),
(3, 3, 10, 8, 1, '2018-10-08 16:02:51');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `item_desc` varchar(1000) NOT NULL,
  `item_image` varchar(50) DEFAULT NULL,
  `location_id` int(11) NOT NULL,
  `item_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item_name`, `item_desc`, `item_image`, `location_id`, `item_status`) VALUES
(1, 'item 1', 'Deluxe Sympathy Fruit Basket—This abundant basket of fresh fruit, selected and hand-packaged by our designers will certainly show how much you care. Fruits such as kiwis, pineapples, grapefruits, apples or plums are beautifully arranged.', 'image_1.jpg', 1, 1),
(2, 'item 2', 'Our Super Holiday Basket Gift Basket is filled to the brim with delicious Gourmet treats. With cookies, crackers, cheese, sausage, candies and dried fruits your family, co-workers, friends and business associates will be delighted.', 'image_2.jpg', 1, 1),
(5, 'item 3', 'Our deluxe Cookie Basket includes, Dolcetto Petites Chocolate Pillow, Terzetto Bakery’s Chocolate Salted Caramel Cookies, Angelina’s Sweet Butter Cookies, Colcetto Chocolate Wafer, Hot Fudge Brownie Bown & Haley Chocolate Covered Graham, Moravian Chocolate Cookies, Chamberry Chocolate Crackle Cookies, Janis & Melanie Triple Chocolate Cookies, Vision Pack Chocolate Wafer Cookies, Miss Grace’s Lemon Cookies, Beth’s Chocolate Chip Cookies, Silk’s Chocolate Crunch Cookies, Walkers Pure Butter Shortbread, Cream Puffs, Amertti Cookies, Biscoff’s Caramelized Beligian Cookies, Mandy’s Chocolate Waffle Crunch Bar, Torani’s Chocolate Milani Biscotti and finished with Passport Confections Chocolate Cookie Crisps.', 'image_3.jpg', 1, 1),
(6, 'item 4', 'Our Rock’in Bye Baby rocking chair set is a stunning way to welcome a newborn into the world.\r\n\r\nOur brown wicker rocking chair is filled with wonderful and useful baby items for the newborn.\r\n\r\nIncluded in the gift;\r\n\r\nA large brown or white teddy bear\r\n\r\ntwo blankets\r\n\r\ntwo outfits\r\n\r\nrobe with slippers and hooded towel with wash clothes\r\n\r\nrattle, bath toy, book, ball\r\n\r\nAll wrapped up with a beautiful organza ribbon', 'image_4.jpg', 1, 1),
(7, 'item 5', 'The Bountiful Gourmet gift basket includes 1.4 oz. J.Morgan’s Licorice Petites, 3.25 oz. Lake Champlain Dark Chocolate Bar, .68 oz. GuyLian Milk Chocolate Truffles, 2 oz. Smoked Almonds, 3.5 oz.  Sonoma Cheese Monterey Jack, 3.5 oz. Kettle Kitchen Peppermint Popcorn, Ernest Hemingway tea, 2 oz. Cranberry Harvest Medley, 2.5 oz. Epicure Double Chocolate Raspberry Cake, 2 oz. Fontazzi Butter Toffee Pretzels, 1.27 Brown & Haley Cashew Roca, 1.5 oz. Olive Oil Potato Chips, 3/4 oz. Partners Toasted Sesame Crackers and finished with 4.4 oz. Angelina’s Sweet Butter Cookies.', 'image_5.jpg', 1, 1),
(8, 'item 6', 'The Happy Birthday Ultimate Chocolate Basket is perfect for anyone with a sweet tooth. Basket includes Funky Chunky Mix, Lindor truffles, Slim Jim, Popcornopolis Popcorn, Triple Nut Chocolate Bar, Firecracker Chocolate Bar, Reeses Peanut Butter Cups, Kit Kat, and Happy Birthday Pasta', 'image_6.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `item_tickets`
--

CREATE TABLE `item_tickets` (
  `transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_tickets`
--

INSERT INTO `item_tickets` (`transaction_id`, `customer_id`, `item_id`, `timestamp`) VALUES
(1, 3, 2, '2018-10-08 16:04:00'),
(2, 3, 2, '2018-10-08 16:04:05'),
(3, 3, 5, '2018-10-08 16:04:11'),
(4, 3, 5, '2018-10-08 16:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `location_id` int(11) NOT NULL,
  `location_abbr` varchar(10) NOT NULL,
  `location_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`location_id`, `location_abbr`, `location_name`) VALUES
(1, 'gso', 'Greensboro, NC');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `customer_email` (`customer_email`);

--
-- Indexes for table `customer_tickets`
--
ALTER TABLE `customer_tickets`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `item_tickets`
--
ALTER TABLE `item_tickets`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`location_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customer_tickets`
--
ALTER TABLE `customer_tickets`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `item_tickets`
--
ALTER TABLE `item_tickets`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
