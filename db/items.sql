-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql:3306
-- Generation Time: Oct 16, 2018 at 10:21 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `raffle`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `item_desc` varchar(1000) NOT NULL,
  `item_image` varchar(50) DEFAULT NULL,
  `location_id` int(11) NOT NULL,
  `item_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item_name`, `item_desc`, `item_image`, `location_id`, `item_status`) VALUES
(1, 'Oh Happy Day', '<ul>\r\n<li>$262 Gift card to Balance Day Spa</li>\r\n<li>Bath Pillow</li>\r\n<li>Bath Brush</li>\r\n<li>24K Gold facial sheet mask</li>\r\n<li>Collagen face mask</li>\r\n<li>BB&amp;W Aromatherapy: body wash &amp; foam, shower steamers, body lotion, body cream</li>\r\n<li>Plush robe</li>\r\n<li>Bath fizzers &amp; salts</li>\r\n<li>Pedi Perfect</li>\r\n<li>Facial cleansing Brush</li>\r\n<li>Sleep mask</li>\r\n<li>Godiva chocolate</li>\r\n</ul>', 'oh_happy_day.jpg', 1, 1),
(2, 'Happy Fall Yall', 'Sponsored By: Underwriting &amp; New Business Teams\r\n<ul>\r\n<li>Decoractive Pillow</li>\r\n<li>Pumpking Arrangement</li>\r\n<li>Pumpkin Monkey Bread Mix</li>\r\n<li>Decoractive Pumpkin</li>\r\n<li>Fall Kitchen Towel</li>\r\n<li>Pumpkin Platter</li>\r\n<li>Pumpkin Yankee Candle</li>\r\n<li>Be Grateful Sign</li>\r\n</ul>', 'happy_fall_yall.jpg', 1, 1),
(5, 'Game Night', 'Sponsored By: The Contract Change & Reinstatements Dept.\r\n<ul>\r\n<li>Whirley Pop 3-min popcorn popper with seasonings, popcorn and oil</li>\r\n<li>2 x $25 Domino&#39;s gift cards</li>\r\n<li>1 x $20 Cold Stone Creamery gift card</li>\r\n<li>Movie &quot;Honey, I Shunk the Kids&quot;</li>\r\n<li>Movie &quot;Barnyard...The Original Party Animals&quot;</li>\r\n<li>Connect 4 game</li>\r\n<li>Uno card game</li>\r\n<li>Twister</li>\r\n<li>Yahtzee</li>\r\n<li>Jenga</li>\r\n<li>Sorry</li>\r\n<li>Trouble</li>\r\n<li>Candy Land</li>\r\n<li>Let&#39;s Go Fishing</li>\r\n<li>8 Boxes of movie candy</li>\r\n<li>Bag of M&M&#39;s</li>\r\n</ul>', 'game_night.jpg', 1, 1),
(6, 'Weekend Getaway', 'Sponsored By: The Inforace Underwriting Dept.\r\n<ul>\r\n<li>Hand Sanitizer</li>\r\n<li>Travel Suitcase</li>\r\n<li>Throw Blanket</li>\r\n<li>Neck Pillow</li>\r\n<li>2 Lincoln beach towels</li>\r\n<li>Lincoln water bottle</li>\r\n<li>Golf balls & tees</li>\r\n<li>Travel grooming kit</li>\r\n<li>Travel size refillable bottles</li>\r\n<li>Cracker Barrel small peg game</li>\r\n<li>2 Travel mugs</li>\r\n<li>2 Sleep masks</li>\r\n<li>2 Loofas</li>\r\n<li>$25 Restaurant gift card</li>\r\n</ul>', 'weekend_getaway.jpg', 1, 1),
(7, 'Gadgets Galore', 'Sponsored By: New Business Technology, Vendor Mgmt Team and Road Runner\r\n<ul>\r\n<li>LETSCOM Fitness Tracker - activity tracker with heart rate monitor</li>\r\n<li>GoWISE USA 3.7 quart programmable air fryer</li>\r\n<li>Anker 10W Wireless Charger - Qi-Certified wireless charging pad (iPhone, Samsung, more)</li>\r\n<li>Halo Bolt 57720 Mwh portable phone charger power bank car jump starter</li>\r\n<li>Oontz Angle 3 enhanced stereo edition portable bluetooth speaker</li>\r\n<li>Zmodo EZCam 720p HD IP camera wi-fi home security surveillance camera system</li>\r\n<li>Tile Mate 4-pack bluetooth tracker - key finder, phone finder, anything finder</li>\r\n<li>DROCON training drone for beginners with real-time video</li>\r\n<li>$50 Amazon Gift Card</li>\r\n<li>Google home mini</li>\r\n</ul>', 'gadgets_galore.jpg', 1, 1),
(8, 'All American', 'Sponsored By: VETNET BRG - Greensboro\r\n<ul>\r\n<li>3&#39;x5&#39; Sewn American flag</li>\r\n<li>$25 Sheetz gift card</li>\r\n<li>$40 Good Ol&#39; American Cash</li>\r\n<li>American-themed baseball cap</li>\r\n<li>$20 Regal Cinemas Gift Card</li>\r\n<li>American Tervis Tumbler</li>\r\n<li>Framed Abraham Lincoln as Captain America</li>\r\n<li>America&#39;s Great National Forest, Wilderness, and Grasslands coffee table book</li>\r\n<li>Luminar Outdoor Lantern</li>\r\n<li>Red Bird Peppermint Candy (Made in USA)</li>\r\n</ul>', 'all_american.jpg', 1, 1),
(9, 'The Coffee Shop', 'Sponsored By: The Direct Agent Team of New Business and Underwriting<ul><li>Keurig K-Classic</li><li>Keurig rolling storage drawer</li><li>2 x 20oz YETI tumblers</li><li>2 x Pottery coffee mugs (made in Seagrove, NC)</li><li>5 varieties of coffee creamers</li><li>1 Box hot chocolate K-Cups</li><li>1 Box Green Mt. hot apple cider K-Cups</li><li>1 Box of 42 Coffee K-Cups</li><li>Black serving tray</li></ul>', 'coffee_shop.jpg', 1, 1),
(10, 'Pizza Night', 'Sponsored By: The Life Financial Management and Strategy Team <ul> <li>$75 Harris Teeter Gift Card</li> <li>Nordic Ware AirBake non-stick large pizza pan silver</li> <li>2 Corckcicle Tumblers</li> <li>Williams Sonoma Goldtouch nonstick deep-dish pizza pan</li> <li>Golden Body pizza sauce</li> <li>Redd Wood roasted green onion chile pizza oil</li> <li>Williams Sonoma pizza crust mix</li> <li>Williams Sonoma pizza seasoning</li> <li>Williams Sonoma pizza night cookbook</li> <li>Pizza Scizza</li> </ul>', 'pizza_night.jpg', 1, 1),
(11, 'Four Hundred &amp; Change', 'Sponsored By: The Claims Department <ul> <li>Piggy Bank</li> <li>Bulldog Bank</li> <li>Safe Deposit Box</li> <li>Pearl Necklace</li> <li>6 toy cards</li> <li>2 Half-dollars</li> <li>4 Susan B. Anthony dollar coins</li> <li>4 x $100 bills</li> </ul>', 'four_hundred.jpg', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
