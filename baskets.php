<?php

	include_once("incConstants.php");

	$arrItems = getAllItems();
	$page = "items";
	
	#ob_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>LFG &amp; United Way Basket Raffle</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.min.css" rel="stylesheet">
  <style type="text/css">
    html,
    body,
    header,
    .carousel {
      height: 60vh;
    }

    @media (max-width: 740px) {
      html,
      body,
      header,
      .carousel {
        height: 100vh;
      }
    }

    @media (min-width: 800px) and (max-width: 850px) {
      html,
      body,
      header,
      .carousel {
        height: 100vh;
      }
    }

    main div.container {
        margin-top: 75px;
    }
  </style>
</head>

<body>

  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container">

      <!-- Brand -->
      <a class="navbar-brand waves-effect" href="https://mdbootstrap.com/material-design-for-bootstrap/" target="_blank">
        <strong class="elegant-text">United Way Basket Raffle</strong>
      </a>

      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- Left -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link waves-effect" href="home.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link waves-effect" href="baskets.php" target="_blank">Baskets</a>
          </li>
          <li class="nav-item">
            <a class="nav-link waves-effect" href="buy.php" target="_blank">Buy Tickets</a>
          </li>
          <li class="nav-item">
            <a class="nav-link waves-effect" href="about.php" target="_blank">About the Raffle</a>
          </li>
        </ul>

        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
          <li class="nav-item">
            <a class="nav-link waves-effect">
              <span class="badge red z-depth-1 mr-1"> 1 </span>
              <i class="fa fa-shopping-cart"></i>
              <span class="clearfix d-none d-sm-inline-block"> Tickets </span>
            </a>
          </li>
          <li class="nav-item">
            <a href="logoff.php" class="nav-link waves-effect" target="_blank">
              <i class="fa fa-facebook"></i> Log Off
            </a>
          </li>
        </ul>

      </div>

    </div>
  </nav>
  <!-- Navbar -->


  <!--Main layout-->
  <main>
    <div class="container h-100">

      <!--Section: Products v.3-->
      <section class="text-center">
          <h2>Enter to Win a Prize Basket!</h2>

        <!--Grid row-->
        <div class="row wow fadeIn">
<?php foreach($arrItems as $item){ 
	$itemTotalTickets = (int)getItemTotalTickets($item['item_id']);
?>
          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card h-100">

              <!--Card image-->
              <div class="view overlay">
                <img src="items/<?=$item['item_image']?>" class="card-img-top" alt="<?=$item['item_name']?>">

              </div>
              <!--Card image-->

              <!--Card content-->
              <div class="card-body text-center">
                <!--Category & Title-->
                <a href="" class="grey-text">
                  <h5><?=$item['item_name']?></h5>
                </a>
                <h5>
                  <strong>
                <?php 
                    if($loggedIn){
                        $cntCustomerItemTickets = (int)getCustomerItemTickets($_SESSION['customerID'],$item['item_id']);
                        echo '<hr><a class="btn btn-outline-light-green" href="enter_drawing.php?itemID='.$item['item_id'].'">Enter to Win</a>';
                        if($cntCustomerItemTickets>0){ echo "<h6>You've entered ".$cntCustomerItemTickets." times.</h6>"; }
                    } else {
                        echo '<a class="btn btn-outline-light-green" href="get_email.php">Register to Win</a>';
                    }
                ?>


                  </strong>
                </h5>

                <h6>
                  <strong><?=$itemTotalTickets?> Entries</strong>
                </h6>

              </div>
              <!--Card content-->

            </div>
            <!--Card-->

          </div>
          <!--Grid column-->
<?php } ?>

        </div>
        <!--Grid row-->

      </section>
      <!--Section: Products v.3-->


    </div>
  </main>
  <!--Main layout-->

  <!--Footer-->
  <footer class="page-footer text-center font-small mt-4 wow fadeIn">

    <!--Call to action-->
    <div class="pt-4">
      <a class="btn btn-outline-white" href="https://mdbootstrap.com/getting-started/" target="_blank" role="button">BUY TICKETS!
        <i class="fa fa-download ml-2"></i>
      </a>
      <a class="btn btn-outline-white" href="https://mdbootstrap.com/bootstrap-tutorial/" target="_blank" role="button">HOW TO HELP
        <i class="fa fa-graduation-cap ml-2"></i>
      </a>
    </div>
    <!--/.Call to action-->

    <hr class="my-4">


    <!--Copyright-->
    <div class="footer-copyright py-3">
      © 2018 Copyright:
      <a href="https://mdbootstrap.com/bootstrap-tutorial/" target="_blank"> Website Courtesy of LFG Infrastructure Team </a>
    </div>
    <!--/.Copyright-->

  </footer>
  <!--/.Footer-->


<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div id="modalalert" class="alert alert-danger"></div>
                <div class="md-form mb-5">
                    <i class="fa fa-envelope prefix grey-text"></i>
                    <input type="email" id="defaultForm-email" name="email" class="form-control validate" required>
                    <label data-error="not valid email" data-success="thanks" for="defaultForm-email">Your email</label>
                </div>


            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button id="modal_submit" class="btn btn-default" type="submit">Login</button>
            </div>
        </div>
    </div>
</div>

<div class="text-center">
    <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Launch Modal Login Form</a>
</div>


  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Initializations -->
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();

$(document).ready(function(){  
      $("#modalalert").hide();
      $('#modal_submit').click(function(){  
           var email = $('#defaultForm-email').val();  
           if(email != '')  
           {  
                $.ajax({  
                     url:"ajax_functions.php?fn=emailcheck",  
                     method:"POST",  
                     data: {email:email}, 
                     dataType: 'json', 
                     success:function(data)  
                     {  
                          //alert(data);  
                          if(data.status == 'err')  
                          {  
                               $("#modalalert").html(data.msg);
                               $("#modalalert").show(); 
                          }  
                          else  
                          {  
                            $("#modalalert").html(data.msg);
                            $("#modalalert").show(); 
                               //$('#modalLoginForm').hide();  
                               //location.reload();  
                          }  
                     }  
                });  
           }  
           else  
           {  
                alert("Both Fields are required");  
           }  
      });  
      $('#logout').click(function(){  
           var action = "logout";  
           $.ajax({  
                url:"action.php",  
                method:"POST",  
                data:{action:action},  
                success:function()  
                {  
                     location.reload();  
                }  
           });  
      });  
 });  
  </script>
</body>

</html>