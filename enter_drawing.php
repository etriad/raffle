<?php

    include_once("incConstants.php");
    $err = false;

    #if no itemID or user is not logged in, back to items
    if(!isset($get['itemID']) || !$loggedIn){ 

        header("Location: items.php"); 

    } else {

		$itemID = $get['itemID'];
		$item = getItem($itemID);

		if(isset($post['itemID']) && $remainingTickets>0){
			$numtickets = $post['numtickets'];
			$i=1;
			while($i<=$numtickets){
				$transactionID = addItemTicket($_SESSION['customerID'],$post['itemID']);
				$i++;
			}
			header("Location:items.php");
		}

        $cntCustomerItemTickets = getCustomerItemTickets($_SESSION['customerID'],$itemID);
        if($totalTickets>0 && $remainingTickets<=0){
            $err = true;
            $errMsg = "You don't have any remaining tickets. Please purchase more to enter this drawing.";
        }

    }


	ob_start();
?>

			<div class="text-center">
			<?php if($err){ echo "<h4>".$errMsg."</h4>";} ?>
			</div>
			<div class="row">
            <div class="col-lg-6 col-md-8 offset-lg-3 offset-md-2">
					<div class="card h-100">
						<img class="card-img-top" alt="<?php echo $item['item_name']; ?>" src="items/<?php echo $item['item_image']; ?>">
						<div class="card-body">
							<h5 class="card-title">
								<?php echo $item['item_name']; ?>
							</h5>
							<p class="card-text">
								<?php echo $item['item_desc']; ?>
							</p>
							<p>
								<?php 
									if($remainingTickets<=0){
										echo '<a class="btn btn-primary" href="buy_tickets.php">Purchase tickets to enter.</a>';
										echo '<a href="items.php" class="btn">Return to Items</a>';
									} else {
										?>
										<form role="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?itemID=<?php echo $itemID; ?>">
											<input type="hidden" name="itemID" value="<?php echo $itemID; ?>">
											<?php if($cntCustomerItemTickets>0){ echo '<h4>You have entered this raffle '.$cntCustomerItemTickets.' times.</h4>'; }?>
											<div class="row">
												<div class="col-xs-12 col-lg-4">
													<div class="form-group">
														<select class="form-control" id="numtickets" name="numtickets">
														<?php for($i=1;$i<=$remainingTickets;$i++){
															if($i==1){ $strTicket = "ticket"; } else { $strTicket = "tickets";}
															echo "<option value='$i'>$i $strTicket</option>";
														}
														?>
														</select>
													</div>
												</div>
												<div class="col-xs-12 col-lg-4">
													<button type="submit" class="btn btn-primary">
														Enter to Win!
													</button>
												</div>
											</div><!--end row-->

											<a href="items.php" class="btn">Return to Items</a>
										</form>
									<?php
									}
								?>
							</p>
						</div>
					</div>
				</div>
			</div>


<?php
	$content = ob_get_clean();
	include_once('mainLayout.php');
?>