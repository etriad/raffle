<?php
	if(isset($get['sid'])){
		$shipmentID = $get['sid'];
		$arrShipment = getShipment($shipmentID);
		$shipment = $arrShipment[0];
	}
	
	$arrStatus = getStatusTypes();


	function getAllContacts(){
		global $db;
		
		$query = $db->prepare("SELECT * FROM CONTACTS ORDER BY CONTACT_ID ASC");
		$query->execute();
		$result = $query->fetchAll();
		return $result;	
	}

	function getShippingTypes(){
		global $db;
		$shippingTypeDomestic = 1;
			
		$query = $db->prepare("SELECT * FROM SHIPPING_TYPES WHERE SHIPPING_TYPE_DOMESTIC = :SHIPPING_TYPE_DOMESTIC ORDER BY SHIPPING_TYPE_NAME ASC");
		$query->bindValue(":SHIPPING_TYPE_DOMESTIC",$shippingTypeDomestic);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	function getStatusTypes(){
		global $db;
			
		$query = $db->prepare("SELECT * FROM STATUS_TYPES ORDER BY STATUS_ID ASC");
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}
	
	function getStatusCount($status){
		global $db;
		
		$query = $db->prepare("SELECT COUNT(*) FROM SHIPMENTSX WHERE STATUS = :status");
		$query->bindValue(":status",$status);
		$query->execute();
		$result = $query->fetch();
		$count = $result[0];
		return $count;	
	}


	function getPieceTypes(){
		global $db;
		$query = $db->prepare("SELECT * FROM PIECE_TYPES ORDER BY PIECE_TYPE_NAME ASC");
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	function getPieceAnimals(){
		global $db;
		$query = $db->prepare("SELECT * FROM PIECE_ANIMALS ORDER BY PIECE_ANIMAL_NAME ASC");
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}


	function getShipmentPieces($shipmentID){
		global $db;
		$query = $db->prepare("SELECT * FROM SHIPMENT_PIECES A, PIECE_TYPES B, PIECE_ANIMALS C WHERE A.SHIPMENT_ID = :shipment_id AND B.PIECE_TYPE_ID = A.PIECE_TYPE_ID AND C.PIECE_ANIMAL_ID = A.PIECE_ANIMAL_ID ORDER BY PIECE_ID ASC");
		$query->bindValue(":shipment_id",$shipmentID);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}
	
	
	function deleteShipmentPieces($shipmentID){
		global $db;
		$query = $db->prepare("DELETE FROM SHIPMENT_PIECES WHERE SHIPMENT_ID = :shipment_id");
		$query->bindValue(":shipment_id",$shipmentID);
		$query->execute();	
	}
	
	
	function getShipment($id){
		global $db;
			
		$query = $db->prepare("SELECT * FROM SHIPMENTSX WHERE SHIPMENT_ID = :shipment_id LIMIT 1");
		$query->bindValue(":shipment_id",$id);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}
	
	function getShipments($status='active'){
		global $db;
			
		$query = $db->prepare("SELECT * FROM SHIPMENTSX WHERE STATUS = :shipment_status ORDER BY MODIFY_DATE DESC");
		$query->bindValue(":shipment_status",$status);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}	

	function getAllShipments(){
		global $db;
			
		$query = $db->prepare("SELECT * FROM SHIPMENTSX ORDER BY MODIFY_DATE DESC");
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}
	
?>