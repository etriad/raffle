<?php
	include_once("incConstants.php");
  secureAdmin();
  
  $arrItems = getAllItems();
	
	ob_start();
?>
<h2>Raffle Items</h2>
<table id="data" class="display" style="width:100%">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Entries</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($arrItems as $item){ 
        $itemTotalEntries = getItemTotalTickets($item['item_id']);
    ?>
    <tr>
      <th scope="row"><?=$item['item_id']?></th>
      <td><a href="admin_item_detail.php?iid=<?=$item['item_id']?>"><?=$item['item_name']?></a></td>
      <td><?=$itemTotalEntries?></td>

    </tr>
  <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Entries</th>
    </tr>
  </tfoot>
</table>

<script type="text/javascript" class="init">
	

    $(document).ready(function() {
        $('#data').DataTable();
    } );
    
    
</script>

<?php
	$content = ob_get_clean();
	include_once('adminLayout.php');
?>
