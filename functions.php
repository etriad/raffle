<?php
# function list 

function secure(){
    if(!isset($_SESSION['customer_id']) ){
        header("location: home.php");
    }
}

function secureAdmin(){
    if(!isset($_SESSION['admin_id']) ){
        header("location: admin.php");
    }
}

function getAllItems(){
    global $db;
    $sql = "select * from items where item_status = 1";
    $query = $db->prepare($sql);
    $query->execute();
    $arrItems = $query->fetchAll();
    $query->closeCursor();
    return $arrItems;
}

function getItem($itemID){
    global $db;
    $sql = "select * from items where item_id = :item_id";
    $query = $db->prepare($sql);
    $query->bindValue("item_id",$itemID);
    $query->execute();
    $item = $query->fetch();
    $query->closeCursor();
    return $item;
}

function getItemTotalTickets($itemID){
    global $db;
    $sql = "select count(*) as totalTickets from item_tickets where item_id = :item_id";
    $query = $db->prepare($sql);
    $query->bindValue("item_id",$itemID);
    $query->execute();
    $result = $query->fetch();
    $query->closeCursor();
    return $result['totalTickets'];
}

function getItemTickets($itemID){
    global $db;
    $sql = "select * from item_tickets a, items b, customers c where a.item_id = b.item_id and a.customer_id = c.customer_id and a.item_id = :item_id";
    $query = $db->prepare($sql);
    $query->bindValue("item_id",$itemID);
    $query->execute();
    $result = $query->fetchAll();
    $query->closeCursor();
    return $result;
}

function getCustomerByEmail($email){
    global $db;
    $sql = "select * from customers where customer_email = :email";
    $query = $db->prepare($sql);
    $query->bindValue("email",strtolower($email));
    $query->execute();
    $customer = $query->fetch();
    $query->closeCursor();
    return $customer;
}

function getCustomer($customerID){
    global $db;
    $sql = "select * from customers where customer_id = :customer_id";
    $query = $db->prepare($sql);
    $query->bindValue("customer_id",$customerID);
    $query->execute();
    $customer = $query->fetch();
    $query->closeCursor();
    return $customer;
}

function checkCustomerCode($customerID,$code){
    global $db;
    $sql = "select * from customers where customer_id = :customer_id and customer_code = :customer_code";
    $query = $db->prepare($sql);
    $query->bindValue("customer_id",$customerID);
    $query->bindValue("customer_code",$code);
    $query->execute();
    $customer = $query->fetch();
    $query->closeCursor();
    return $customer;
}

function getCustomerTicketStats(){
    global $db;
    $sql = "select sum(ticket_cnt) as total_tickets, sum(transaction_amt) as total_amt, count(distinct customer_id) as num_customers from customer_tickets";
    $query = $db->prepare($sql);
    $query->execute();
    $results = $query->fetch();
    $query->closeCursor();
    return $results;    
}

function getTransactionStats(){
    global $db;
    $sql = "select count(distinct customer_id) as cnt_customers, count(*) as cnt_transactions from item_tickets";
    $query = $db->prepare($sql);
    $query->execute();
    $results = $query->fetch();
    $query->closeCursor();
    return $results;    
}

function getCustomerwithUnused(){
    global $db;
    $sql = "select * from customers where customer_id in (select customer_id from customer_tickets) and not in (select customer_";
    $query = $db->prepare($sql);
    $query->execute();
    $results = $query->fetch();
    $query->closeCursor();
    return $results;    
}

function getCustomerTickets($customerID){
    global $db;
    $sql = "select * from customer_tickets a, admins b where a.admin_id = b.admin_id and customer_id = :customer_id";
    $query = $db->prepare($sql);
    $query->bindValue("customer_id",$customerID);
    $query->execute();
    $customerTickets = $query->fetchAll();
    $query->closeCursor();
    return $customerTickets;    
}

function getAllCustomerTickets(){
    global $db;
    $sql = "select * from customer_tickets a, customers b, admins c where a.customer_id = b.customer_id and a.admin_id = c.admin_id order by ticket_timestamp asc";
    $query = $db->prepare($sql);
    $query->execute();
    $customerTickets = $query->fetchAll();
    $query->closeCursor();
    return $customerTickets;    
}

function getCustomerTicketTotal($customerID){
    global $db;
    $sql = "select sum(ticket_cnt) as total_cnt, sum(transaction_amt) as total_amt from customer_tickets where customer_id = :customer_id";
    $query = $db->prepare($sql);
    $query->bindValue("customer_id",$customerID);
    $query->execute();
    $result = $query->fetch();
    $query->closeCursor();
    return $result;    
}

function getCustomerTicketsUsed($customerID){
    global $db;
    $sql = "select count(*) as used_tickets from item_tickets where customer_id = :customer_id";
    $query = $db->prepare($sql);
    $query->bindValue("customer_id",$customerID);
    $query->execute();
    $customerTicketsUsed = $query->fetch();
    $query->closeCursor();
    return $customerTicketsUsed['used_tickets'];    
}

function getCustomerItemTickets($customerID,$itemID){
    global $db;
    $sql = "select count(*) as item_tickets from item_tickets where customer_id = :customer_id and item_id = :item_id";
    $query = $db->prepare($sql);
    $query->bindValue("customer_id",$customerID);
    $query->bindValue("item_id",$itemID);
    $query->execute();
    $result = $query->fetch();
    $query->closeCursor();
    return $result['item_tickets']; 
}

function getCustomeritem_tickets($customerID){
    global $db;
    $sql = "select * from item_tickets a, items b where a.item_id = b.item_id and customer_id = :customer_id";
    $query = $db->prepare($sql);
    $query->bindValue("customer_id",$customerID);
    $query->execute();
    $result = $query->fetchAll();
    $query->closeCursor();
    return $result;    
}

function generateCode(){
    $rand = substr(md5(microtime()),rand(0,26),4);
    return $rand;
}

function addCustomer($email,$code,$fname,$lname){
    global $db;
    $sql = "insert into customers (customer_email,customer_code,customer_fname,customer_lname,customer_confirmed,location_id) values (:email,:customerCode,:fname,:lname,0,1)";
    $query = $db->prepare($sql);
    $query->bindValue("email",$email);
    $query->bindValue("customerCode",$code);
    $query->bindValue("fname",$fname);
    $query->bindValue("lname",$lname);
    $query->execute();
    $customerID = $db->lastInsertID();
    $query->closeCursor();
    return $customerID;
}

function addItemTicket($customerID,$itemID){
    global $db;
    $customerCode = generateCode();
    $sql = "insert into item_tickets (customer_id,item_id) values (:customerID,:itemID)";
    $query = $db->prepare($sql);
    $query->bindValue("customerID",$customerID);
    $query->bindValue("itemID",$itemID);
    $query->execute();
    $transactionID = $db->lastInsertID();
    $query->closeCursor();
    return $transactionID;
}

function addCustomerTicket($customerID,$email,$ticketCnt,$amt,$adminID){
    global $db;
    $sql = "insert into customer_tickets (customer_id,ticket_cnt,transaction_amt,admin_id) values (:customerID,:ticketCnt,:amt,:adminID)";
        $query = $db->prepare($sql);
        $query->bindValue("customerID",$customerID);
        $query->bindValue("ticketCnt",$ticketCnt);
        $query->bindValue("amt",$amt);
        $query->bindValue("adminID",$adminID);
        $query->execute();
        $query->closeCursor();
        $transactionID = $db->lastInsertID();
        sendEmail('transaction',array("to"=>$email,"cnt"=>$ticketCnt,"amt"=>$amt));
        return $transactionID;
}




function getAdmin($email,$code){
    global $db;
    $sql = "select * from admins where admin_email = :email and admin_code = :code";
    $query = $db->prepare($sql);
    $query->bindValue("email",$email);
    $query->bindValue("code",$code);
    $query->execute();
    $result = $query->fetch();
    $query->closeCursor();
    return $result;    
}

function pullItemWinner($itemID){
    global $db;
    $sql = "select * from item_tickets a, customers b where a.customer_id = b.customer_id and a.item_id = :item order by RAND() limit 1";
    $query = $db->prepare($sql);
    $query->bindValue("item",$itemID);
    $query->execute();
    $result = $query->fetch();
    $query->closeCursor();
    return $result;
}


function sendEmail($type,$arrData){
    $emailFrom = "donotreply@lfg.com";
    $header = $emailFrom;
    switch ($type) {

        case 'customerCode':
            $subject = "Your United Way Raffle Code";
            $body = "Here is your private code for the united way raffle. Good luck!: ".$arrData['code'];
            break;

        case 'transaction':
            $subject = "Your raffle tickets";
            $body = "You purchased ".$arrData['cnt']." tickets and donated $".$arrData['amt'];
            break;

        case 'enter_drawing':
        $subject = "You are entered to win";
        $body = "You entered to win ".$arrData['item_name'];
        break;


    }

        if(mail($arrData['to'],$subject,$body,$header)){
            return true;
        } else {
            return false;
        }
}


?>