<?php

    include_once("incConstants.php");
    $err = false;
        if(isset($post['email'])){
            $email = strtolower($post['email']);
            if(preg_match('/@lfg\.com$/i',$email) || preg_match('/@lfd\.com$/i',$email)){

                $arrCustomer = getCustomerByEmail($email);

                if($arrCustomer){
                    $customerID = $arrCustomer['customer_id'];
                    $_SESSION['customerID'] = $customerID;
                    $_SESSION['customer_email'] = $arrCustomer['customer_email'];
                } else {
                    $code = generateCode();
                    $customerID = addCustomer($email,$code,'','');
                    if($customerID){
                        sendEmail('customerCode',array("to"=>$email,"code"=>$code));
                        $_SESSION['customer_email'] = $email;
                        $_SESSION['codeSent'] = true;
                        $_SESSION['code'] = $code;
                        $_SESSION['customerID'] = $customerID;
                    }
                } #end if is customer
                header("Location:get_code.php");

            } else {
                $err = true;
                $errMsg =  "Sorry, Only LFG email addresses allowed to participate.";
            }#end if lfg email
        }#end post['email'] check


	ob_start();
?>


        <div class="row">
            <div class="col-lg-8">
                <h3>Please enter your Lincoln email.</h3>
                <?php if($err){ echo '<p class="alert alert-danger">'.$errMsg.'</p>';} ?>
            <form role="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
				<div class="form-group">

					<label for="inputEmail">
						Email address
					</label>
					<input name="email" type="email" class="form-control" id="inputEmail" required>
				</div>
				<button type="submit" class="btn btn-primary">
					Submit Your LFG Email Address
				</button>
            </form>
            </div>
        </div>


<?php
	$content = ob_get_clean();
	include_once('mainLayout.php');
?>