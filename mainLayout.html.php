<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Trophy Transport</title>
<!-- Latest compiled and minified CSS -->

<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/cupertino/jquery-ui.min.css">

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.maskedinput.min.js"></script>
   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
      <script src="../assets/js/respond.min.js"></script>
    <![endif]-->

<style type="text/css">
	fieldset {
		margin-bottom:20px;
	}
	legend {
		font-size:18px;
		color:#666;
	}
	input[type=search] {
		border-radius:20px;
	}
	label {
		font-size:14px;
		font-weight:bold;
		color:#666;
	}
</style>
</head>

<body>

    <!-- Static navbar -->
    <div class="navbar navbar-default navbar-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Trophy Transport Mgmt</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="dashboard.php">Dashboard</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shipments <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="shipment_add.php">Add Shipment</a></li>
                <li class="divider"></li>
                <?php foreach($arrStatus as $statusList){ 
					$statusCount = getStatusCount($statusList['STATUS_NAME']);
				?>
                <li><a href="shipment_list.php?status=<?=$statusList['STATUS_NAME']?>"><?=ucfirst($statusList['STATUS_NAME'])?> Shipments [<?=$statusCount?>]</a></li>
                <?php } ?>
                <li class="divider"></li>
                <li><a href="shipment_list.php">All Shipments</a></li>
              </ul>
            </li>
			<li><a href="masterbill_list.php">Master Bills</a></li>
            <li><a href="contact_list.php">Contacts</a></li>
            <li><a href="quote_requests.php">Quote Requests</a></li>
            <li><a href="routes.php">Routes</a></li>
            <li><a href="trucks.php">Trucks</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>


    <div class="container">

	<?=$content?>

	<div class="clearfix"><br /></div>
</div><!--/.container -->
    
</body>
</html>
