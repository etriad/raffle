<?php
include_once("incConstants.php");
secure();
    
    $err = false;

    if(isset($post['submitted'])){
        $email = strtolower($post['email']);
        $fname = $post['fname'];
        $lname = $post['lname'];
        if(preg_match('/@lfg\.com$/i',$email)){

            $arrCustomer = getCustomerByEmail($email);

            if($arrCustomer){
                $customerID = $arrCustomer['customer_id'];
                $_SESSION['customerID'] = $customerID;
            } else {
                $code = generateCode();
                $customerID = addCustomer($email,'','');
                if(sendEmail('customerCode',array("to"=>$email,"code"=>$code))){
                    $_SESSION['codeSent'] = true;
                    $_SESSION['code'] = $code;
                }
            } #end if is customer
            $_SESSION['email'] = $email;
            header("Location:get_code.php");

        } else {
            $err = true;
            $errMsg =  "Sorry, Only LFG email addresses allowed to participate.";
        }#end if lfg email
    }#end post['email'] check


	ob_start();
?>

            <div class="row">
            <div class="col-md-6">
            <h3>Please enter you LFG email address.</h3>
            <?php if($err){ echo "<h4>".$errMsg."</h4>";} ?>
            <form role="form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <input type="hidden" name="submitted" value="Y">
				<div class="form-group">
					<label for="inputEmail">
						Email address
					</label>
					<input name="email" type="email" class="form-control" id="inputEmail">
				</div>
				<div class="form-group">
					<label for="inputFname">
						First Name
					</label>
					<input name="fname" type="text" class="form-control" id="inputFname">
				</div>
				<div class="form-group">
					<label for="inputLname">
						Last Name
					</label>
					<input name="lname" type="test" class="form-control" id="inputLname">
				</div>

				<button type="submit" class="btn btn-primary">
					Submit
				</button>
            </form>

            </div>
            </div>

<?php
	$content = ob_get_clean();
	include_once('mainLayout.php');
?>