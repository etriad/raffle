<?php

    include_once("incConstants.php");
    $err = false;
        if(isset($post['email'])){
            $email = strtolower($post['email']);
            if(preg_match('/@lfg\.com$/i',$email)){

                $arrCustomer = getCustomerByEmail($email);

                if($arrCustomer){
                    $customerID = $arrCustomer['customer_id'];
                    $_SESSION['customerID'] = $customerID;
                    $_SESSION['customer_email'] = $arrCustomer['customer_email'];
                } else {
                    $code = generateCode();
                    $customerID = addCustomer($email,$code,'','');
                    if(sendEmail('customerCode',array("to"=>$email,"code"=>$code))){
                        $_SESSION['customer_email'] = $email;
                        $_SESSION['codeSent'] = true;
                        $_SESSION['code'] = $code;
                    }
                } #end if is customer
                header("Location:get_code.php");

            } else {
                $err = true;
                $errMsg =  "Sorry, Only LFG email addresses allowed to participate.";
            }#end if lfg email
        }#end post['email'] check


	ob_start();
?>

    <div class="row">
      <div class="col-lg-10 col-xl-9 mx-auto">
        <div class="card card-signin flex-row my-5">
          <div class="card-img-left d-none d-md-flex">
             <!-- Background image for card set in CSS! -->
          </div>
          <div class="card-body">
            <h5 class="card-title text-center">Register</h5>
            <form class="form-signin">
              <div class="form-label-group">
              <label for="inputUserame">Item Name</label>
                <input type="text" id="inputUserame" class="form-control" placeholder="Item Name" required autofocus>
              </div>

              <div class="form-label-group">
              <label for="inputEmail">Item Desc</label>
                <textarea id="inputEmail" class="form-control" placeholder="Long Desc" ></textarea>
              </div>
              
              <hr>

              <div class="form-label-group">
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                <label for="inputPassword">Password</label>
              </div>
              
              <div class="form-label-group">
                <input type="password" id="inputConfirmPassword" class="form-control" placeholder="Password" required>
                <label for="inputConfirmPassword">Confirm password</label>
              </div>

              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Register</button>
              <a class="d-block text-center mt-2 small" href="#">Sign In</a>
              <hr class="my-4">
              <button class="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i class="fab fa-google mr-2"></i> Sign up with Google</button>
              <button class="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i class="fab fa-facebook-f mr-2"></i> Sign up with Facebook</button>
            </form>
          </div>
        </div>
      </div>
    </div><?php
	$content = ob_get_clean();
	include_once('mainLayout.php');
?>