<?php

	include_once("incConstants.php");

	$arrItems = getAllItems();
	$page = "view_baskets";
	
	ob_start();
?>


  <!--Main layout-->
  <main>
    <div class="container">

      <!--Section: Products v.3-->
      <section class="text-center">
          <h2>Enter to Win a Prize Basket!</h2>

        <!--Grid row-->
        <div class="row wow fadeIn">
<?php foreach($arrItems as $item){ 
	$itemTotalTickets = (int)getItemTotalTickets($item['item_id']);
?>
          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card h-100">

              <!--Card image-->
              <div class="view overlay">
               <a href="basket.php?itemID=<?=$item['item_id']?>"><img src="items/<?=$item['item_image']?>" class="card-img-top" alt="<?=$item['item_name']?>"  data-toggle="popover" title="<?=$item['item_name']?>" data-content="<?=$item['item_desc']?></a>">
              </div>
              <!--Card image-->

              <!--Card content-->
              <div class="card-body text-center">
                <!--Category & Title-->
                <a href="" class="grey-text">
                  <h5><?=$item['item_name']?></h5>
                </a>
                <h5>
                  <strong>
                <?php 
                    if($loggedIn){
                        $cntCustomerItemTickets = (int)getCustomerItemTickets($_SESSION['customerID'],$item['item_id']);
                        if($remainingTickets>0){ echo '<hr><a class="btn btn-dark-green" href="basket.php?itemID='.$item['item_id'].'">Enter to Win!</a>';
                        } else {
                        echo '<hr><a class="btn warning-color-dark" href="buy_tickets.php?itemID='.$item['item_id'].'">Buy Tickets</a>';
                        }
                        if($cntCustomerItemTickets>0){ echo "<h6>You've entered ".$cntCustomerItemTickets." times.</h6>"; }
                    } else {
                        echo '<a class="btn btn-outline-light-green" href="home.php">Login to Enter</a>';
                    }
                ?>


                  </strong>
                </h5>

                <h6>
                  <strong>Total Entries: <?=$itemTotalTickets?></strong>
                </h6>

              </div>
              <!--Card content-->

            </div>
            <!--Card-->

          </div>
          <!--Grid column-->
<?php } ?>

        </div>
        <!--Grid row-->

      </section>
      <!--Section: Products v.3-->


    </div>
  </main>
  <!--Main layout-->



<?php
	$content = ob_get_clean();
	include_once('layout.php');
?>