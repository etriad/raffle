<?php

	include_once("incConstants.php");

    $arrItems = getAllItems();
    $codeSent = false;

    if(isset($post['loginTask'])){
        switch($post['loginTask']){

            case 'email':
            $email = strtolower($post['email']);
            if(substr($email,-8)!='@lfg.com'){
                echo "has to be lfg!";
            } else {
                $arrCustomer = getCustomerByEmail($email);

                if($arrCustomer){
                    $customerID = $arrCustomer['customer_id'];
                    $_SESSION['customerID'] = $customerID;
                    $ticketCnt = getCustomerTicketTotal($customerID);
                    echo 'Total Tickets:'.$ticketCnt;
                    $usedTickets = getCustomerTicketsUsed($customerID);
                    echo 'Used Tickets: '.$usedTickets;
                    echo 'Remaining Tickets: '.((int)$ticketCnt-(int)$usedTickets);
                    echo $_SESSION['customerID'];
                } else {
                    $code = generateCode();
                    if(sendEmail('customerCode',array("to"=>$email,"code"=>$code))){
                        $codeSent = true;
                    }
                }
            }
            break;

            case 'code':

                echo "code was submitted".$post['code'];
            break;
        }
    }
		
	ob_start();
?>

    
            <?php if(!isset($_SESSION['customerID'])){ ?>
            <h3>Please enter you LFG email address.</h3>			
            <form role="form" method="post" action="login.php">
                <input type="hidden" name="loginTask" value="email">
				<div class="form-group">
					 
					<label for="inputEmail">
						Email address
					</label>
					<input name="email" type="email" class="form-control" id="inputEmail">
				</div>
				<button type="submit" class="btn btn-primary">
					Submit
				</button>
            </form>
    
            <?php } else { ?>

            <?php if($codeSent){ echo "code sent"; } else { ?>

            <form role="form" method="post" action="login.php">
                <input type="hidden" name="loginTask" value="code">
				<div class="form-group">
					 
					<label for="inputCode">
						Code
					</label>
					<input name="code" type="text" class="form-control" id="inputCode">
				</div>
				<button type="submit" class="btn btn-primary">
					Submit
				</button>
            </form>
            <?php }} ?>
    <script>
		$( "#login_form" ).submit(function( event ) {
		
			// Stop form from submitting normally
			event.preventDefault();
			
			// Get some values from elements on the page:
			var $form = $( this ),
				url = "ajaxFunctions.php?fn=login";
			
			// Send the data using post
			var posting = $.post( url, $form.serialize(), function( data ){
				var json = $.parseJSON(data);
				if(json.status=='fail'){
					$(".field").addClass("error");
					$("#loginMsg").html('Invalid username / password.');
				} else if(json.status=='success'){
					window.location = json.requrl;
				}
			});
		
		});
    </script>
<?php
	$content = ob_get_clean();
	include_once('mainLayout.php');
?>