<?php

    include_once("incConstants.php");
    $err = false;
        if(isset($post['email'])){
            $email = strtolower($post['email']);
            if(preg_match('/@lfg\.com$/i',$email) || preg_match('/@lfd\.com$/i',$email)){

                $arrCustomer = getCustomerByEmail($email);

                if($arrCustomer){
                    $customerID = $arrCustomer['customer_id'];
                    $_SESSION['customerID'] = $customerID;
                    $_SESSION['customer_email'] = $arrCustomer['customer_email'];
                } else {
                    $code = generateCode();
                    $customerID = addCustomer($email,$code,'','');
                    if($customerID){
                        sendEmail('customerCode',array("to"=>$email,"code"=>$code));
                        $_SESSION['customer_email'] = $email;
                        $_SESSION['codeSent'] = true;
                        $_SESSION['code'] = $code;
                        $_SESSION['customerID'] = $customerID;
                    }
                } #end if is customer
                header("Location:get_code.php");

            } else {
                $err = true;
                $errMsg =  "Sorry, Only LFG email addresses allowed to participate.";
            }#end if lfg email
        }#end post['email'] check


	ob_start();
?>

<div class="row">
        <div class="col-md-12">
            <h2 class="text-center text-white mb-4">Bootstrap 4 Login Form</h2>
            <div class="row">
                <div class="col-md-6 mx-auto">

                    <!-- form card login -->
                    <div class="card rounded-0">
                        <div class="card-header">
                            <h3 class="mb-0">Admin Login</h3>
                        </div>
                        <div class="card-body">
                            <form class="form" role="form" autocomplete="off" id="formLogin" novalidate="" method="POST">
                                <div class="form-group">
                                    <label for="uname1">LFG Email Address</label>
                                    <input type="email" class="form-control form-control-lg rounded-0" name="email" id="uname1" required>
                                    <div class="invalid-feedback">Oops, you missed this one.</div>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="text" class="form-control form-control-lg rounded-0" id="pwd1" name="code" required autocomplete="new-password">
                                    <div class="invalid-feedback">Enter your access code too!</div>
                                </div>
                                <div>
                                    <label class="custom-control custom-checkbox">
                                      <input type="checkbox" class="custom-control-input">
                                      <span class="custom-control-indicator"></span>
                                      <span class="custom-control-description small text-dark">Remember me on this computer</span>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-success btn-lg float-right" id="btnLogin">Login</button>
                            </form>
                        </div>
                        <!--/card-block-->
                    </div>
                    <!-- /form card login -->

                </div>


            </div>
            <!--/row-->

        </div>
        <!--/col-->
    </div>
    <!--/row-->

<?php
	$content = ob_get_clean();
	include_once('mainLayout.php');
?>