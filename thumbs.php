<?php

	include_once("incConstants.php");

	$arrItems = getAllItems();

/*
	if(isset($get['requrl']) && strlen($get['requrl']>0)){
		$requrl = $get['requrl'];
	} else {
		$requrl = 'dashboard.php';
	}
	
	if($loggedIn==true){
		header("Location: dashboard.php");
	}
*/	
		
	ob_start();
?>
			<h1>
				United Way Raffle
			</h1>

			<div class="row">
<?php foreach($arrItems as $item){ ?>
				<div class="col-md-4">
					<div class="card">
						<img class="card-img-top" alt="<?php echo $item['item_name']; ?>" src="items/<?php echo $item['item_image']; ?>">
						<div class="card-block">
							<h5 class="card-title">
								Card title
							</h5>
							<p class="card-text">
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
							<p>
								<?php 
									if($loggedIn){
										echo '<a class="btn btn-primary" href="enter.php">Enter to Win</a>';
									} else {
										echo '<a class="btn btn-primary" href="login.php">Register to Win</a>';
									}
								?>
							</p>
						</div>
					</div>
				</div>
<?php } ?>
			</div>
	<script>
		$( "#login_form" ).submit(function( event ) {
		
			// Stop form from submitting normally
			event.preventDefault();
			
			// Get some values from elements on the page:
			var $form = $( this ),
				url = "ajaxFunctions.php?fn=login";
			
			// Send the data using post
			var posting = $.post( url, $form.serialize(), function( data ){
				var json = $.parseJSON(data);
				if(json.status=='fail'){
					$(".field").addClass("error");
					$("#loginMsg").html('Invalid username / password.');
				} else if(json.status=='success'){
					window.location = json.requrl;
				}
			});
		
		});
    </script>
<?php
	$content = ob_get_clean();
	include_once('mainLayout.php');
?>
